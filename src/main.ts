import { HTTP_INTERCEPTORS, provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { enableProdMode, importProvidersFrom, provideExperimentalZonelessChangeDetection } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { bootstrapApplication } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { provideRouter } from '@angular/router';
import { AppMainComponent } from './app/app.component';
import { APP_ROUTES } from './app/app.routes';
import { AddAcceptLanguageHeaderInterceptor } from './app/interceptors/add-accept-language-header.interceptor';
import { CacheGetRequestsInterceptor } from './app/interceptors/cache-get-requests.interceptor';
import { CatchRequestsErrorsInterceptor } from './app/interceptors/catch-requests-errors.interceptor';
import { AddProtocolUrlPipe } from './app/pipes/add-protocol-url.pipe';
import { SimplifyUrlPipe } from './app/pipes/simplify-url.pipe';
import { TruncatePipe } from './app/pipes/truncate.pipe';
import { TranslocoRootModule } from './app/transloco/transloco-root.module';
import { environment } from './environments/environment';
import { ApiModule } from './generated/api.module';


if (environment.production) {
  enableProdMode();
}

bootstrapApplication(AppMainComponent, {
  providers: [
    importProvidersFrom(
      ApiModule.forRoot({
        rootUrl: environment.backendUrl.replace(/\/$/, ''),
      }),
      BrowserAnimationsModule,
      ReactiveFormsModule,
      TranslocoRootModule,
    ),
    provideRouter(APP_ROUTES),
    AddProtocolUrlPipe,
    SimplifyUrlPipe,
    TruncatePipe,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AddAcceptLanguageHeaderInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CacheGetRequestsInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CatchRequestsErrorsInterceptor,
      multi: true,
    },
    provideHttpClient(withInterceptorsFromDi()),
    provideExperimentalZonelessChangeDetection(),
  ]
}).catch(err => console.error(err));
