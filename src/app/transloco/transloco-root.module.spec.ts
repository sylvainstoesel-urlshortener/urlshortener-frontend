import { provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { HttpTestingController, provideHttpClientTesting } from '@angular/common/http/testing';
import { provideExperimentalZonelessChangeDetection } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { Translation } from '@jsverse/transloco';
import { Observable } from 'rxjs';
import { TranslocoHttpLoader } from './transloco-root.module';

describe('TranslocoHttpLoader', () => {
  let translocoLoader: TranslocoHttpLoader;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
    imports: [],
    providers: [
      TranslocoHttpLoader,
      provideHttpClient(withInterceptorsFromDi()),
      provideHttpClientTesting(),
      provideExperimentalZonelessChangeDetection(),]
});
    httpTestingController = TestBed.inject(HttpTestingController);
    translocoLoader = TestBed.inject(TranslocoHttpLoader);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(translocoLoader).toBeTruthy();
  });

  it('getTranslation() should retrieve a translation', () => {
    // [Given]
    const dataToBeReturned = { textToTranslate: 'English text' };
    const language = 'en';

    // [When]
    const translation: Observable<Translation> = translocoLoader.getTranslation(language);

    // [Then]
    translation.subscribe((data) => expect(data).toEqual(dataToBeReturned));
    const requestsDone = httpTestingController.expectOne(`/assets/i18n/${language}.json`);
    expect(requestsDone.request.method).toEqual('GET');
    requestsDone.flush(dataToBeReturned);
  });
});
