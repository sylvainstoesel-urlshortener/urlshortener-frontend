import { HttpClient } from '@angular/common/http';
import { Injectable, NgModule } from '@angular/core';
import { Translation, TranslocoLoader, TranslocoModule, provideTransloco } from '@jsverse/transloco';
import { TranslocoLocaleModule, provideTranslocoLocale } from '@jsverse/transloco-locale';
import { provideTranslocoMessageformat } from '@jsverse/transloco-messageformat';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

export const DEFAULT_LANGUAGE = 'en';
export const AVAILABLE_LANGUAGES_CONST = [DEFAULT_LANGUAGE, 'fr', 'es', 'it', 'de'] as const;
export const AVAILABLE_LANGUAGES = AVAILABLE_LANGUAGES_CONST.map((s) => s as string);

@Injectable({ providedIn: 'root' })
export class TranslocoHttpLoader implements TranslocoLoader {
  constructor(private httpClient: HttpClient) { }

  getTranslation(lang: string): Observable<Translation> {
    return this.httpClient.get<Translation>(`/assets/i18n/${lang}.json`);
  }
}

@NgModule({
  exports: [TranslocoModule, TranslocoLocaleModule],
  providers: [
    provideTransloco({
      loader: TranslocoHttpLoader,
      config: {
        availableLangs: AVAILABLE_LANGUAGES,
        defaultLang: DEFAULT_LANGUAGE,
        fallbackLang: DEFAULT_LANGUAGE,
        reRenderOnLangChange: true,
        prodMode: environment.production,
      }
    }),
    provideTranslocoLocale({
      langToLocaleMapping: {
        en: 'en-US',
        fr: 'fr-FR',
        es: 'es-ES',
        it: 'it-IT',
        de: 'de-DE',
      }
    }),
    provideTranslocoMessageformat(),
  ]
})
export class TranslocoRootModule { }
