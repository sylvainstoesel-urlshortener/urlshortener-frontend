import { NgModule } from '@angular/core';
import { TranslocoTestingModule } from '@jsverse/transloco';
import { TranslocoLocaleModule, provideTranslocoLocale } from '@jsverse/transloco-locale';
import { provideTranslocoMessageformat } from '@jsverse/transloco-messageformat';
import de from '../../assets/i18n/de.json';
import en from '../../assets/i18n/en.json';
import es from '../../assets/i18n/es.json';
import fr from '../../assets/i18n/fr.json';
import it from '../../assets/i18n/it.json';

@NgModule({
  imports: [
    TranslocoTestingModule.forRoot({
      langs: { en, fr, de, it, es },
      translocoConfig: {
        availableLangs: ['en', 'fr', 'de', 'it', 'es'],
        defaultLang: 'en',
      },
      preloadLangs: true,
    })
  ],
  exports: [TranslocoTestingModule, TranslocoLocaleModule],
  providers: [
    provideTranslocoLocale({
      langToLocaleMapping: {
        en: 'en-US',
        fr: 'fr-FR',
        es: 'es-ES',
        it: 'it-IT',
        de: 'de-DE',
      }
    }),
    provideTranslocoMessageformat(),
  ]
})
export class TranslocoTestRootModule { }