import { UrlSegment } from '@angular/router';
import { APP_ROUTES, isShortUrlId } from './app.routes';

describe('AppRoutes', () => {

    it('isShortUrlId should detect URL short IDs', () => {
        expect(isShortUrlId([])).toBeFalsy();
        expect(isShortUrlId([new UrlSegment('', {})])).toBeFalsy();
        expect(isShortUrlId([new UrlSegment('123', {})])).toBeFalsy();
        expect(isShortUrlId([new UrlSegment('++abc!de-', {})])).toBeFalsy();

        expect(isShortUrlId([new UrlSegment('1a2b3c4d5e', {})])).toBeTruthy();
        expect(isShortUrlId([new UrlSegment('0123456789', {})])).toBeTruthy();
        expect(isShortUrlId([new UrlSegment('abcdefghij', {})])).toBeTruthy();
    });

    it('ROUTES should be initialized', () => {
        expect(APP_ROUTES.length).toEqual(4);
    });
});