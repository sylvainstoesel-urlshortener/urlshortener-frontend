import { LocationStrategy } from '@angular/common';
import { provideHttpClient } from '@angular/common/http';
import { MockLocationStrategy } from '@angular/common/testing';
import { provideExperimentalZonelessChangeDetection } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Title } from '@angular/platform-browser';
import { provideRouter } from '@angular/router';
import { AppMainComponent } from './app.component';
import { APP_ROUTES } from './app.routes';
import { GoogleAnalyticsService } from './services/google-analytics.service';
import { UrlShortenerStore } from './store/url-shortener.store';
import { TranslocoTestRootModule } from './transloco/transloco-testing.module.spec';

describe('AppMainComponent', () => {
  let component: AppMainComponent;
  let fixture: ComponentFixture<AppMainComponent>;

  let spyGoogleAnalyticsService: jasmine.SpyObj<GoogleAnalyticsService>;
  let spyStore: jasmine.Spy;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        AppMainComponent, TranslocoTestRootModule,
      ],
      providers: [
        Title,
        UrlShortenerStore,
        provideHttpClient(),
        { provide: LocationStrategy, useClass: MockLocationStrategy },
        { provide: GoogleAnalyticsService, useValue: jasmine.createSpyObj('GoogleAnalyticsService', ['logMainScreenLoad']) },
        provideRouter(APP_ROUTES),
        provideExperimentalZonelessChangeDetection(),
      ]
    }).compileComponents();
    
    spyStore = spyOn(TestBed.inject(UrlShortenerStore), 'loadMainPage');
    spyGoogleAnalyticsService = TestBed.inject(GoogleAnalyticsService) as jasmine.SpyObj<GoogleAnalyticsService>;
    fixture = TestBed.createComponent(AppMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    // [When] / [Then]
    expect(component).toBeTruthy();
    expect(component.serverErrorMsg()).toEqual('');
    expect(component.isLoading()).toEqual(false);
    expect(spyGoogleAnalyticsService.logMainScreenLoad.calls.count()).toEqual(1);
    expect(spyStore.calls.count()).toEqual(1);
  });
});
