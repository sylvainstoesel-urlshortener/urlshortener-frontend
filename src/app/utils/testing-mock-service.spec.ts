import { UrlDetails, UrlParameters } from '../../generated/models';
import { ShortUrlDetailsPage } from '../models/short-url-details-page.model';

export function generateApiUrlParameters(): UrlParameters {
  return { shortUrlIdSize: 10, shortUrlDomain: 'short.ca/', apiVersion: '1.0.0' };
}

export function generateUrlDetails(idSuffix?: number): UrlDetails {
  return {
    shortUrlId: `abcde0000${idSuffix}`,
    shortUrl: `short.com/abcde0000${idSuffix}`,
    longUrl: `long-url-${idSuffix}.com`,
    creationTimestamp: '',
  };
}

export function generateShortUrlDetailsPage(): ShortUrlDetailsPage {
  return new ShortUrlDetailsPage(1, 5, 20, 4, [
    generateUrlDetails(0),
    generateUrlDetails(1),
    generateUrlDetails(2),
    generateUrlDetails(3),
    generateUrlDetails(4),
  ]);
}

export function generateError(status: number): {
  status: number;
  message: string;
} {
  return {
    status,
    message: `Error with ${status} status`,
  };
}
