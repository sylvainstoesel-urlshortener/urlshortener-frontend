import { Pipe, PipeTransform } from '@angular/core';

// Pipe used to remove 'http://' or 'https://' at the beginning of a URL.
@Pipe({
  name: 'simplifyUrl',
  standalone: true
})
export class SimplifyUrlPipe implements PipeTransform {

  transform(value: string): string {
    if (value) {
      return value.replace(/https?:\/\//i, '');
    }
    return value;
  }

}
