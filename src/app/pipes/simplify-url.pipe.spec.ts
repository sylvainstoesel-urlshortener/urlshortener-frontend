import { SimplifyUrlPipe } from './simplify-url.pipe';

describe('SimplifyUrlPipe', () => {
  const pipe: SimplifyUrlPipe = new SimplifyUrlPipe();

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should return empty value unchanged', () => {
    const url = '';
    expect(pipe.transform(url)).toEqual(url);
  });

  it('should remove http prefix from URL', () => {
    const url = 'www.url.com';
    expect(pipe.transform('http://' + url)).toEqual(url);
  });

  it('should remove https prefix from URL', () => {
    const url = 'www.url.com';
    expect(pipe.transform('https://' + url)).toEqual(url);
  });
});
