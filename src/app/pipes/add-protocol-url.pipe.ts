import { Pipe, PipeTransform } from '@angular/core';

// Pipe used to add 'https://' at the beginning of a URL.
@Pipe({
  name: 'addProtocolUrl',
  standalone: true
})
export class AddProtocolUrlPipe implements PipeTransform {

  transform(value: string): string {
    if (value && !value.startsWith('http://') && !value.startsWith('https://')) {
      return 'http://' + value;
    }
    return value;
  }

}
