import { TruncatePipe } from './truncate.pipe';

describe('TruncatePipe', () => {
  const pipe: TruncatePipe = new TruncatePipe();

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should return empty value unchanged', () => {
    const text = '';
    expect(pipe.transform(text)).toEqual(text);
  });

  it('should return complete value whose length is less than 25 characters', () => {
    const text = 'Short text less than 25';
    expect(pipe.transform(text)).toEqual(text);
  });

  it('should return truncated value whose length is more than 25 characters', () => {
    expect(pipe.transform('Text of more than 25 characters')).toEqual('Text of more than 25 char...');
  });

  it('should return truncated value whose length is more than 25 characters, with complete words', () => {
    expect(pipe.transform('This is a text of more than 25 characters', 25, true)).toEqual('This is a text of more...');
  });


  it('should return truncated value whose length is more than 15 characters with custom ellipsis', () => {
    expect(pipe.transform('This is a text of more than 15 characters', 15, false, '###')).toEqual('This is a text ###');
  });
});
