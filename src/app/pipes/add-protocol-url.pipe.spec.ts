import { AddProtocolUrlPipe } from './add-protocol-url.pipe';

describe('AddProtocolUrlPipe', () => {
  const pipe: AddProtocolUrlPipe = new AddProtocolUrlPipe();

  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should return empty value unchanged', () => {
    const url = '';
    expect(pipe.transform(url)).toEqual(url);
  });

  it('should return URL starting with http unchanged', () => {
    const url = 'http://www.url.com/';
    expect(pipe.transform(url)).toEqual(url);
  });


  it('should return URL starting with https unchanged', () => {
    const url = 'https://www.url.com/';
    expect(pipe.transform(url)).toEqual(url);
  });


  it('should return URL with http prefix when not starting with http or https', () => {
    const url = 'www.url.com';
    expect(pipe.transform(url)).toEqual('http://' + url);
  });
});
