import { HTTP_INTERCEPTORS, HttpClient, provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { HttpTestingController, provideHttpClientTesting } from '@angular/common/http/testing';
import { provideExperimentalZonelessChangeDetection, signal } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { UrlShortenerStore } from '../store/url-shortener.store';
import { TranslocoTestRootModule } from '../transloco/transloco-testing.module.spec';
import { AddAcceptLanguageHeaderInterceptor } from './add-accept-language-header.interceptor';

describe('AddAcceptLanguageHeaderInterceptor', () => {
  let interceptor : AddAcceptLanguageHeaderInterceptor;
  let client: HttpClient;
  let httpMock: HttpTestingController;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ TranslocoTestRootModule ],
      providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AddAcceptLanguageHeaderInterceptor,
            multi: true,
        },
        provideHttpClient(withInterceptorsFromDi()),
        provideHttpClientTesting(),
        {
          provide: UrlShortenerStore,
          useValue: {
            language: signal('de'),
          },
        },
        provideExperimentalZonelessChangeDetection(),
      ]
    }).compileComponents();
    
    interceptor = TestBed.inject(HTTP_INTERCEPTORS)[0] as AddAcceptLanguageHeaderInterceptor;
    client = TestBed.inject(HttpClient);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });


  it('should be created', () => {
    // [When] / [Then]
    expect(interceptor).toBeTruthy();
    expect(interceptor instanceof AddAcceptLanguageHeaderInterceptor).toBeTrue();
  });


  it('should add an Accept-Language header on an HTTP request', () => {
    // [Given]
    const endpoint = '/endpoint';

    // [When]
    client.get<any>(endpoint).subscribe((response) => {
      expect(response).toBeTruthy();
    });

    // [Then]
    const httpRequest = httpMock.match(endpoint)[0];
    expect(httpRequest.request.headers.get('Accept-Language')).toEqual('de');
  });
});
