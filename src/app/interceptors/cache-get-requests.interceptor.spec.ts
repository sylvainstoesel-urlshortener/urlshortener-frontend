import { HTTP_INTERCEPTORS, HttpClient, provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { HttpTestingController, provideHttpClientTesting } from '@angular/common/http/testing';
import { provideExperimentalZonelessChangeDetection } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { EMPTY } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { TranslocoTestRootModule } from '../transloco/transloco-testing.module.spec';
import { CacheGetRequestsInterceptor } from './cache-get-requests.interceptor';

describe('CacheGetRequestsInterceptor', () => {

  let interceptor : CacheGetRequestsInterceptor;
  let client: HttpClient;
  let httpMock: HttpTestingController;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TranslocoTestRootModule],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: CacheGetRequestsInterceptor,
          multi: true,
        },
        provideHttpClient(withInterceptorsFromDi()),
        provideHttpClientTesting(),
        provideExperimentalZonelessChangeDetection(),
      ],
    }).compileComponents();

    interceptor = TestBed.inject(HTTP_INTERCEPTORS)[0] as CacheGetRequestsInterceptor;
    client = TestBed.inject(HttpClient);
    httpMock = TestBed.inject(HttpTestingController);    
  });

  afterEach(() => {
    httpMock.verify();
  });


  it('should be created', () => {
    // [Then]
    expect(interceptor).toBeTruthy();
    expect(interceptor instanceof CacheGetRequestsInterceptor).toBeTrue();
  });


  it('should cache result of a GET request', () => {
    // [Given]
    const endpoint = '/endpoint';
    
    // [When]
    client.get<any>(endpoint).subscribe((response) => {
      expect(response).toBeTruthy();
    });
    // [Then]
    httpMock.expectOne(endpoint);
    expect(interceptor.store.size).toEqual(1);
    expect(interceptor.store.get(endpoint)).toBeTruthy();
    
    // Retry request using cache
    client.get<any>(endpoint).subscribe((response) => {
      expect(response).toBeTruthy();
    });
    httpMock.expectNone(endpoint);
  });

  
  it('should remove from cache the result of a GET request after an error response', async () => {
    // [Given]
    const endpoint = '/endpoint';

    // [When]
    client.get<any>(endpoint)
      .pipe(catchError(err => {
        expect(err.message).toBeTruthy();
        return EMPTY;
      }))
      .subscribe((response) => {
        console.log(response); // should not happen
      });

      // [Then]
    const requestDone = httpMock.expectOne(endpoint);
    expect(interceptor.store.size).toEqual(1);
    expect(interceptor.store.get(endpoint)).toBeTruthy();
    
    requestDone.flush({message: 'Error'}, {status: 500, statusText: 'Internal Server Error'});
    expect(interceptor.store.size).toEqual(0);
    await new Promise( resolve => setTimeout(resolve, 100));
    httpMock.expectNone(endpoint);
  });


  it('should not cache result of a POST request', () => {
    // [Given]
    const endpoint = '/endpoint';
    
    // [When]
    client.post<any>(endpoint, null).subscribe((response) => {
      expect(response).toBeTruthy();
    });
    // [Then]
    httpMock.expectOne(endpoint);
    expect(interceptor.store.size).toEqual(0);
    
    // Retry request using cache
    client.post<any>(endpoint, null).subscribe((response) => {
      expect(response).toBeTruthy();
    });
    httpMock.expectOne(endpoint);
    expect(interceptor.store.size).toEqual(0);
  });

  
  it('should remove everything from cache after a POST request', () => {
    // [Given]
    const endpoint0 = '/endpoint0';
    const endpoint1 = '/endpoint1';
    const endpoint2 = '/endpoint2';
    
    client.get<any>(endpoint1).subscribe((response) => {
      expect(response).toBeTruthy();
    });
    httpMock.expectOne(endpoint1);
    client.get<any>(endpoint2).subscribe((response) => {
      expect(response).toBeTruthy();
    });
    httpMock.expectOne(endpoint2);
    expect(interceptor.store.size).toEqual(2);
    expect(interceptor.store.get(endpoint1)).toBeTruthy();
    expect(interceptor.store.get(endpoint2)).toBeTruthy();

    // [When]
    client.post<any>(endpoint0, null).subscribe((response) => {
      expect(response).toBeTruthy();
    });

    // [Then]
    httpMock.expectOne(endpoint0);
    expect(interceptor.store.size).toEqual(0);
  });
});
