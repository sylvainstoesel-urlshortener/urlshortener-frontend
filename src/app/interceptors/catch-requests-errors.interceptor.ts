import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { Observable, throwError, timer } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { ApiError } from 'src/generated/models';
import { LanguageSelectService } from '../services/language-select.service';

@Injectable()
export class CatchRequestsErrorsInterceptor implements HttpInterceptor {

  private languageSelectService = inject(LanguageSelectService);

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      retry({ count: 1, delay: this.shouldRetry }),
      catchError((error) => this.handleError(error, this)),
    );
  }

  shouldRetry(error: HttpErrorResponse) {
    if (![429, 404].includes(error.status)) {
      console.log(`HTTP Request ${error.url}: error ${error.status}, retrying...`);
      return timer(500); // Adding a 0.5s delay before retrying.
    }
    // If  quota have exceeded or URL is not found, then we don't want to retry, and we can throw the error.
    throw error;
  }

  handleError(error: HttpErrorResponse, thisInstance: CatchRequestsErrorsInterceptor): Observable<never> {
    const outputError: ApiError = {
      status: error.status || -1,
      code : error.error?.code ? `${error.error.code}` : '',
      message : error.error?.message || thisInstance.languageSelectService.getTranslation('app.error-server'),
    };

    console.error(`STATUS ${outputError.status} WHEN CONTACTING API: ${outputError.message}`);
    return throwError(() => outputError);
  }
}
