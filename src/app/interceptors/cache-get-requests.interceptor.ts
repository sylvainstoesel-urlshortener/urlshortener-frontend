import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, filter, first, shareReplay } from 'rxjs/operators';

@Injectable()
export class CacheGetRequestsInterceptor implements HttpInterceptor {
  public readonly store: Map<string, Observable<HttpEvent<any>>> = new Map();

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // If we receive another request than a GET, we reset the whole cache.
    if (req.method !== 'GET') {
      this.store.clear();
      return next.handle(req);
    }

    let cachedObservable = this.store.get(req.urlWithParams);
    if (!cachedObservable) {
      cachedObservable = next.handle(req).pipe(
        filter((res) => res instanceof HttpResponse),
        shareReplay(1),
        catchError((error) => {
          // Let's not cache requests that failed (HTTP status 4xx or 5xx).
          this.store.delete(req.urlWithParams);
          return throwError(() => error);
        })
      );
      this.store.set(req.urlWithParams, cachedObservable);
    }
    return cachedObservable.pipe(first());
  }
}
