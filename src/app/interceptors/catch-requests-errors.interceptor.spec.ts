import { HTTP_INTERCEPTORS, HttpClient, HttpErrorResponse, provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { HttpTestingController, provideHttpClientTesting } from '@angular/common/http/testing';
import { provideExperimentalZonelessChangeDetection } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { EMPTY } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { TranslocoTestRootModule } from '../transloco/transloco-testing.module.spec';
import { CatchRequestsErrorsInterceptor } from './catch-requests-errors.interceptor';

describe('CatchRequestsErrorsInterceptor', () => {

  let interceptor : CatchRequestsErrorsInterceptor;
  let client: HttpClient;
  let httpMock: HttpTestingController;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TranslocoTestRootModule],
      providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: CatchRequestsErrorsInterceptor,
            multi: true,
        },
        provideHttpClient(withInterceptorsFromDi()),
        provideHttpClientTesting(),
        provideExperimentalZonelessChangeDetection(),
      ],
    }).compileComponents();

    interceptor = TestBed.inject(HTTP_INTERCEPTORS)[0] as CatchRequestsErrorsInterceptor;
    client = TestBed.inject(HttpClient);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });


  it('should be created', () => {
    // [Then]
    expect(interceptor).toBeTruthy();
    expect(interceptor instanceof CatchRequestsErrorsInterceptor).toBeTrue();
  });


  it('should catch and retry once a request that always gives a 500 http error', async () => {
    // [Given]
    const endpoint = '/endpoint';

    // [When]
    client.get<any>(endpoint).pipe(catchError(error => {
      expect(error).toBeTruthy();
      expect(error.message).toEqual('Error500');
      expect(error.status).toEqual(500);
      return EMPTY;
    })).subscribe(response => {
      console.log(response); // should not happen
    });

    // [Then]
    var requestDone = httpMock.expectOne(endpoint);
    requestDone.flush({ message: 'Error500' }, { status: 500, statusText: 'Internal Server Error' });

    await new Promise( resolve => setTimeout(resolve, 900)); // Wait a bit before catching second request
    requestDone = httpMock.expectOne(endpoint);
    requestDone.flush({ message: 'Error500' }, { status: 500, statusText: 'Internal Server Error' });

    httpMock.expectNone(endpoint);
  });


  it('should catch and retry successfully a request that gave once a 500 http error', async () => {
    // [Given]
    const endpoint = '/endpoint';
    const dataToBeReturned = { value : '' };

    // [When]
    client.get<any>(endpoint).pipe(catchError(error => {
      console.log(error); // should not happen
      return EMPTY;
    })).subscribe(response => {
      expect(response).toEqual(dataToBeReturned);
    });

    // [Then]
    var requestDone = httpMock.expectOne(endpoint);
    requestDone.flush({ message: 'Error500' }, { status: 500, statusText: 'Internal Server Error' });

    await new Promise( resolve => setTimeout(resolve, 900)); // Wait a bit before catching second request
    requestDone = httpMock.expectOne(endpoint);
    requestDone.flush(dataToBeReturned);

    httpMock.expectNone(endpoint);
  });


  
  it('should not retry a request that gave a 404 http error', async () => {
    // [Given]
    const endpoint = '/endpoint';

    // [When]
    client.get<any>(endpoint).pipe(catchError(error => {
      expect(error).toBeTruthy();
      expect(error.message).toEqual('Error404');
      expect(error.status).toEqual(404);
      return EMPTY;
    })).subscribe(response => {
      console.log(response); // should not happen
    });

    // [Then]
    var requestDone = httpMock.expectOne(endpoint);
    requestDone.flush({ message: 'Error404' }, { status: 404, statusText: 'Not Found' }); 

    await new Promise( resolve => setTimeout(resolve, 900)); // Wait a bit before catching second request
    httpMock.expectNone(endpoint);
  });

  it('handleError() should throw error with status and message from complete HttpErrorResponse', () => {
    // [Given]
    const status = 500;
    const message = 'ErrorMessage!';
    const httpErrorResponse = new HttpErrorResponse({ error: { message, code: '123' }, status });
    // [When] / [Then]
    interceptor.handleError(httpErrorResponse, interceptor).subscribe({
      error: (err) => {
        expect(err).toEqual({ status, code: '123', message });
      }
    });
  });

  
  it('handleError() should throw error with status and message from partial HttpErrorResponse', () => {
    // [Given]
    const httpErrorResponse = new HttpErrorResponse({});
    // [When] / [Then]
    interceptor.handleError(httpErrorResponse, interceptor).subscribe({
      error: (err) => {
        expect(err).toEqual({ status: -1, code: '', message: 'Unable to contact server' });
      }
    });
  });
});
