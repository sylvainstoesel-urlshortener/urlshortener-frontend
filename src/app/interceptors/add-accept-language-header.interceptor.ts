import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UrlShortenerStore } from '../store/url-shortener.store';

@Injectable()
export class AddAcceptLanguageHeaderInterceptor implements HttpInterceptor {

  private store = inject(UrlShortenerStore);
  private language = this.store.language;

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req.clone({ headers: req.headers.set('Accept-Language', this.language()) }));
  }
}
