import { ChangeDetectionStrategy, Component, OnInit, signal, WritableSignal } from '@angular/core';
import { MatProgressSpinner, ProgressSpinnerMode } from '@angular/material/progress-spinner';
import { TranslocoRootModule } from 'src/app/transloco/transloco-root.module';

@Component({
    selector: 'app-loading',
    templateUrl: './loading.component.html',
    styleUrls: ['./loading.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [MatProgressSpinner, TranslocoRootModule]
})
export class LoadingComponent implements OnInit {

  timer = signal(0.0);
  spinnerMode: WritableSignal<ProgressSpinnerMode> = signal('indeterminate');

  ngOnInit(): void {
    setInterval(() => {
      if (this.timer() < 25) {
        this.timer.update((t) => t + 0.25);
      }
      if (this.timer() > 2) {
        this.spinnerMode.set('determinate');
      }
    }, 250);
  }

}
