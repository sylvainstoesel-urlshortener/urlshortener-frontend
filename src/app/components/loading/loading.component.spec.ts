import { provideExperimentalZonelessChangeDetection } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { UrlShortenerStore } from 'src/app/store/url-shortener.store';
import { TranslocoTestRootModule } from 'src/app/transloco/transloco-testing.module.spec';
import { LoadingComponent } from './loading.component';

describe('LoadingComponent', () => {
  let component: LoadingComponent;
  let fixture: ComponentFixture<LoadingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [LoadingComponent, MatProgressSpinnerModule, TranslocoTestRootModule],
      providers: [
        UrlShortenerStore,
        provideExperimentalZonelessChangeDetection(),
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.spinnerMode()).toEqual('indeterminate');
  });

  it('should change spinner mode to "determinate" after 3 seconds', async () => {
    // [Given]
    component.timer.set(1.5);
    // [When]
    await new Promise((resolve) => setTimeout(resolve, 2000));
    // [Then]
    expect(component.spinnerMode()).toEqual('determinate');
  });


  it('should not increase the timer after 25 seconds', async () => {
    // [Given]
    component.timer.set(24.5);
    // [When]
    await new Promise((resolve) => setTimeout(resolve, 1000));
    // [Then]
    expect(component.timer()).toEqual(25);
  });
});
