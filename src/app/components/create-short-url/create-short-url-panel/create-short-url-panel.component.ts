import { ChangeDetectionStrategy, Component } from '@angular/core';
import { UrlDetails } from 'src/generated/models';
import { CreateShortUrlFormComponent } from '../create-short-url-form/create-short-url-form.component';
import { CreateShortUrlResponseComponent } from '../create-short-url-response/create-short-url-response.component';

@Component({
    selector: 'app-create-short-url-panel',
    templateUrl: './create-short-url-panel.component.html',
    styleUrls: ['./create-short-url-panel.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [CreateShortUrlFormComponent, CreateShortUrlResponseComponent]
})
export class CreateShortUrlPanelComponent {

  shortUrlDetailsCreated?: UrlDetails;

  public onFormSubmitted(shortUrlDetailsCreated: UrlDetails): void {
    this.shortUrlDetailsCreated = shortUrlDetailsCreated;
  }

  public onGenerateAnotherUrl(): void {
    this.shortUrlDetailsCreated = undefined;
  }
}
