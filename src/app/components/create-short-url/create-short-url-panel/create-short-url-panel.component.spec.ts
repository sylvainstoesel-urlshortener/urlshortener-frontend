import { provideHttpClient } from '@angular/common/http';
import { provideExperimentalZonelessChangeDetection } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslocoTestRootModule } from 'src/app/transloco/transloco-testing.module.spec';
import { generateUrlDetails } from 'src/app/utils/testing-mock-service.spec';
import { CreateShortUrlPanelComponent } from './create-short-url-panel.component';

describe('CreateShortUrlPanelComponent', () => {
  let component: CreateShortUrlPanelComponent;
  let fixture: ComponentFixture<CreateShortUrlPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CreateShortUrlPanelComponent, TranslocoTestRootModule],
      providers: [
        provideHttpClient(),
        provideExperimentalZonelessChangeDetection(),
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateShortUrlPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('onFormSubmitted() should update the created short URL details', () => {
    // [Given]
    const shortUrlDetailsCreated = generateUrlDetails();
    // [When]
    component.onFormSubmitted(shortUrlDetailsCreated);
    // [Then]
    expect(component.shortUrlDetailsCreated).toEqual(shortUrlDetailsCreated);
  });

  it('onGenerateAnotherUrl() should reset the created short URL details', () => {
    // [Given]
    component.shortUrlDetailsCreated = generateUrlDetails();
    // [When]
    component.onGenerateAnotherUrl();
    // [Then]
    expect(component.shortUrlDetailsCreated).toBeUndefined();
  });
});
