import { Clipboard } from '@angular/cdk/clipboard';
import { provideExperimentalZonelessChangeDetection } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { TranslocoService } from '@jsverse/transloco';
import { TranslocoTestRootModule } from 'src/app/transloco/transloco-testing.module.spec';
import { generateUrlDetails } from 'src/app/utils/testing-mock-service.spec';
import { CreateShortUrlResponseComponent } from './create-short-url-response.component';

describe('CreateShortUrlResponseComponent', () => {
  let component: CreateShortUrlResponseComponent;
  let fixture: ComponentFixture<CreateShortUrlResponseComponent>;

  let spyTranslocoServiceTranslate: jasmine.Spy;
  let spyEventEmitterEmit: jasmine.Spy;
  let spyMatSnackBar: jasmine.SpyObj<MatSnackBar>;
  let spyClipboard: jasmine.SpyObj<Clipboard>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CreateShortUrlResponseComponent, MatSnackBarModule, TranslocoTestRootModule],
      providers: [
        { provide: MatSnackBar, useValue: jasmine.createSpyObj('MatSnackBar', ['open']) },
        { provide: Clipboard, useValue: jasmine.createSpyObj('Clipboard', ['copy']) },
        provideExperimentalZonelessChangeDetection(),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateShortUrlResponseComponent);
    
    spyMatSnackBar = TestBed.inject(MatSnackBar) as jasmine.SpyObj<MatSnackBar>;
    spyClipboard = TestBed.inject(Clipboard) as jasmine.SpyObj<Clipboard>;

    const translocoService = TestBed.inject(TranslocoService) as TranslocoService;
    spyTranslocoServiceTranslate = spyOn(translocoService, 'translate');
    
    component = fixture.componentInstance;
    fixture.detectChanges();

    spyEventEmitterEmit = spyOn(component.whenGenerateAnotherUrl, 'emit');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('copyToClipboard() should not copy short URL to clipboard if not present', () => {
    // [Given]
    component.shortUrlDetailsCreated = undefined;
    // [When]
    component.copyToClipboard();
    // [Then]
    expect(spyClipboard.copy.calls.count()).toEqual(0);
    expect(spyTranslocoServiceTranslate.calls.count()).toEqual(0);
    expect(spyMatSnackBar.open.calls.count()).toEqual(0);

  });

  it('copyToClipboard() should copy short URL to clipboard when present', () => {
    // [Given]
    const translation = 'Copied!';
    spyTranslocoServiceTranslate.and.returnValue(translation);
    component.shortUrlDetailsCreated = generateUrlDetails();

    // [When]
    component.copyToClipboard();

    // [Then]
    expect(spyClipboard.copy.calls.count()).toEqual(1);
    expect(spyClipboard.copy.calls.first().args).toEqual([component.shortUrlDetailsCreated.shortUrl]);
    expect(spyTranslocoServiceTranslate.calls.count()).toEqual(1);
    expect(spyTranslocoServiceTranslate.calls.first().args).toEqual(['create-url.response.copied-clipboard']);
    expect(spyMatSnackBar.open.calls.count()).toEqual(1);
    expect(spyMatSnackBar.open.calls.first().args).toEqual([translation, undefined, {
      horizontalPosition: 'right',
      verticalPosition: 'bottom',
      duration: 2000
    }]);

  });

  it('generateAnotherUrl() should emit an event', () => {
    // [When]
    component.generateAnotherUrl();
    // [Then]
    expect(spyEventEmitterEmit.calls.count()).toEqual(1);
  });
});
