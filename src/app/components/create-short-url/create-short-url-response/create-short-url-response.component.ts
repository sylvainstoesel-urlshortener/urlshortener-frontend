import { Clipboard } from '@angular/cdk/clipboard';
import { ChangeDetectionStrategy, Component, EventEmitter, inject, Input, Output } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslocoService } from '@jsverse/transloco';
import { AddProtocolUrlPipe } from 'src/app/pipes/add-protocol-url.pipe';
import { SimplifyUrlPipe } from 'src/app/pipes/simplify-url.pipe';
import { TranslocoRootModule } from 'src/app/transloco/transloco-root.module';
import { UrlDetails } from 'src/generated/models';

@Component({
    selector: 'app-create-short-url-response',
    templateUrl: './create-short-url-response.component.html',
    styleUrls: ['./create-short-url-response.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [TranslocoRootModule, SimplifyUrlPipe, AddProtocolUrlPipe]
})
export class CreateShortUrlResponseComponent {
  private snackBar = inject(MatSnackBar);
  private translate = inject(TranslocoService);
  private clipboard = inject(Clipboard);

  @Input() shortUrlDetailsCreated?: UrlDetails;
  // To be sent to parent component (panel)
  @Output() whenGenerateAnotherUrl = new EventEmitter<any>();


  copyToClipboard(): void {
    if (this.shortUrlDetailsCreated) {
      this.clipboard.copy(this.shortUrlDetailsCreated.shortUrl);
      this.snackBar.open(this.translate.translate('create-url.response.copied-clipboard'), undefined, {
        horizontalPosition: 'right',
        verticalPosition: 'bottom',
        duration: 2000
      });
    }
  }

  generateAnotherUrl(): void {
    this.whenGenerateAnotherUrl.emit();
  }
}
