import { NgClass, NgIf } from '@angular/common';
import { ChangeDetectionStrategy, Component, EventEmitter, inject, Output, signal } from '@angular/core';
import { FormControl, NonNullableFormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { EMPTY } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CallBackendService } from 'src/app/services/call-backend.service';
import { GoogleAnalyticsService } from 'src/app/services/google-analytics.service';
import { TranslocoRootModule } from 'src/app/transloco/transloco-root.module';

@Component({
    selector: 'app-create-short-url-form',
    templateUrl: './create-short-url-form.component.html',
    styleUrls: ['./create-short-url-form.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [TranslocoRootModule, NgClass, NgIf, MatProgressSpinnerModule, ReactiveFormsModule]
})
export class CreateShortUrlFormComponent {

  static URL_REGEX = /^(https?:\/\/)?([\w\-.]+\.\w+|localhost)(:\d{1,5})?(\/[.\w,()@:%+\-~#?&/=!$]*)?$/i;

  private formBuilder = inject(NonNullableFormBuilder);
  private callBackendService = inject(CallBackendService);
  private googleAnalyticsService = inject(GoogleAnalyticsService);

  // To be sent to parent component (panel) when the form is submitted.
  @Output() whenFormSubmitted = new EventEmitter<any>();

  // The form.
  createShortUrlForm = this.formBuilder.group({
    longUrlValue: this.formBuilder.control('', {
      validators: [Validators.required, Validators.pattern(CreateShortUrlFormComponent.URL_REGEX)]
    })
  });

  isSubmitted = signal(false);
  isLoading = signal(false);
  serverErrorMsg = signal('');

  onLongUrlValueChanged(): void {
    this.isSubmitted.set(false);
    this.serverErrorMsg.set('');
  }

  onSubmit(): void {
    if (this.createShortUrlForm.invalid || this.isSubmitted()) {
      this.isSubmitted.set(true);
      return;
    }
    this.googleAnalyticsService.logMainScreenShortenUrl();
    this.isSubmitted.set(true);
    this.isLoading.set(true);

    this.callBackendService
      .createShortUrlFromLongUrl(this.createShortUrlForm.getRawValue().longUrlValue)
      .pipe(
        catchError(error => {
          this.serverErrorMsg.set(error.message);
          this.isLoading.set(false);
          return EMPTY;
        }))
      .subscribe((data) => {
        this.isSubmitted.set(false);
        this.isLoading.set(false);
        this.whenFormSubmitted.emit(data);
      });
  }


  // Getters.
  get longUrlValue(): FormControl<string> {
    return this.createShortUrlForm.controls.longUrlValue;
  }

  get showValidationErrors(): boolean {
    return (!!this.longUrlValue.errors || !!this.serverErrorMsg()) && this.showIsValid;
  }

  get showIsValid(): boolean {
    return this.longUrlValue.touched || this.isSubmitted();
  }
}
