import { provideExperimentalZonelessChangeDetection } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { of, throwError } from 'rxjs';
import { CallBackendService } from 'src/app/services/call-backend.service';
import { TranslocoTestRootModule } from 'src/app/transloco/transloco-testing.module.spec';
import { generateError, generateUrlDetails } from 'src/app/utils/testing-mock-service.spec';
import { CreateShortUrlFormComponent } from './create-short-url-form.component';


describe('CreateShortUrlFormComponent', () => {
  let component: CreateShortUrlFormComponent;
  let fixture: ComponentFixture<CreateShortUrlFormComponent>;
  let spyCallBackendService: jasmine.SpyObj<CallBackendService>;
  let spyEventEmitterEmit: jasmine.Spy;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CreateShortUrlFormComponent, FormsModule, ReactiveFormsModule, TranslocoTestRootModule],
      providers: [
        { provide: CallBackendService, useValue: jasmine.createSpyObj('CallBackendService', ['createShortUrlFromLongUrl']) },
        provideExperimentalZonelessChangeDetection(),
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    spyCallBackendService = TestBed.inject(CallBackendService) as jasmine.SpyObj<CallBackendService>;
    
    fixture = TestBed.createComponent(CreateShortUrlFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    spyEventEmitterEmit = spyOn(component.whenFormSubmitted, 'emit');
  });

  it('should create', () => {
    // [When] / [Then]
    expect(component).toBeTruthy();
    expect(component.createShortUrlForm).toBeTruthy();
    expect(component.createShortUrlForm.controls.longUrlValue).toBeTruthy();
    expect(component.isSubmitted()).toBeFalse();
    expect(component.isLoading()).toBeFalse();
    expect(component.serverErrorMsg()).toEqual('');
    expect(component.longUrlValue.errors?.required).toBeTruthy();
    expect(component.longUrlValue.errors?.pattern).toBeFalsy();
  });

  it('onSubmit() with valid long URL should submit the form', () => {
    // [Given]
    const urlDetailsReturned = generateUrlDetails();
    component.createShortUrlForm.controls.longUrlValue.setValue('www.url.com');
    spyCallBackendService.createShortUrlFromLongUrl.and.returnValue(of(urlDetailsReturned));
    // [When]
    component.onSubmit();
    // [Then]
    expect(component.longUrlValue.errors).toBeFalsy();
    expect(component.isSubmitted()).toBeFalse();
    expect(component.isLoading()).toBeFalse();
    expect(component.showIsValid).toBeFalse();
    expect(spyCallBackendService.createShortUrlFromLongUrl.calls.count()).toEqual(1);
    expect(spyCallBackendService.createShortUrlFromLongUrl.calls.first().args).toEqual(['www.url.com']);
    expect(spyEventEmitterEmit.calls.count()).toEqual(1);
    expect(spyEventEmitterEmit.calls.first().args).toEqual([urlDetailsReturned]);
  });

  it('onSubmit() with valid long URL should submit the form and return an error when API responds an error', () => {
    // [Given]
    const error = generateError(500);
    component.createShortUrlForm.controls.longUrlValue.setValue('www.url.com');
    spyCallBackendService.createShortUrlFromLongUrl.and.returnValue(throwError(error));
    // [When]
    component.onSubmit();
    // [Then]
    expect(component.longUrlValue.errors).toBeFalsy();
    expect(component.isSubmitted()).toBeTrue();
    expect(component.isLoading()).toBeFalse();
    expect(component.showIsValid).toBeTrue();
    expect(component.serverErrorMsg()).toEqual(error.message);
    expect(spyCallBackendService.createShortUrlFromLongUrl.calls.count()).toEqual(1);
    expect(spyCallBackendService.createShortUrlFromLongUrl.calls.first().args).toEqual(['www.url.com']);
    expect(spyEventEmitterEmit.calls.count()).toEqual(0);
  });

  it('onSubmit() with empty long URL in form should not submit the form', () => {
    // [When]
    component.onSubmit();
    // [Then]
    expect(component.longUrlValue.errors?.required).toBeTruthy();
    expect(component.longUrlValue.errors?.pattern).toBeFalsy();
    expect(component.isSubmitted()).toBeTrue();
    expect(component.isLoading()).toBeFalse();
    expect(component.showIsValid).toBeTrue();
    expect(spyCallBackendService.createShortUrlFromLongUrl.calls.count()).toEqual(0);
  });

  it('onSubmit() with invalid long URL in form should not submit the form', () => {
    // [Given]
    component.createShortUrlForm.controls.longUrlValue.setValue('wwwurlcom');
    // [When]
    component.onSubmit();
    // [Then]
    expect(component.longUrlValue.errors?.required).toBeFalsy();
    expect(component.longUrlValue.errors?.pattern).toBeTruthy();
    expect(component.isSubmitted()).toBeTrue();
    expect(component.isLoading()).toBeFalse();
    expect(component.showIsValid).toBeTrue();
    expect(spyCallBackendService.createShortUrlFromLongUrl.calls.count()).toEqual(0);
  });

  it('onLongUrlValueChanged() should reset isSubmitted and serverErrorMsg values', () => {
    // [Given]
    component.isSubmitted.set(true);
    component.serverErrorMsg.set('Error');
    // [When]
    component.onLongUrlValueChanged();
    // [Then]
    expect(component.isSubmitted()).toBeFalse();
    expect(component.isLoading()).toBeFalse();
  });

  it('get longUrlValue() should return the form control longUrlValue', () => {
    // [When] / [Then]
    expect(component.longUrlValue).toEqual(component.createShortUrlForm.controls.longUrlValue);
  });

  it('get showValidationErrors() should return false by default', () => {
    // [When] / [Then]
    expect(component.showValidationErrors).toBeFalse();
  });

  it('get showValidationErrors() should return true when form is submitted', () => {
    // [Given]
    component.createShortUrlForm.controls.longUrlValue.setValue('www.url.com');
    component.isSubmitted.set(true);
    component.serverErrorMsg.set('Error');
    // [When] / [Then]
    expect(component.showValidationErrors).toBeTrue();
  });

  it('get showIsValid() should return false by default', () => {
    // [When] / [Then]
    expect(component.showIsValid).toBeFalse();
  });
});
