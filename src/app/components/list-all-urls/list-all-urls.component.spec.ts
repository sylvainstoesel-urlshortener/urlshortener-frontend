import { provideExperimentalZonelessChangeDetection } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog, MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { of, throwError } from 'rxjs';
import { AddProtocolUrlPipe } from 'src/app/pipes/add-protocol-url.pipe';
import { SimplifyUrlPipe } from 'src/app/pipes/simplify-url.pipe';
import { CallBackendService } from 'src/app/services/call-backend.service';
import { UrlShortenerStore } from 'src/app/store/url-shortener.store';
import { TranslocoTestRootModule } from 'src/app/transloco/transloco-testing.module.spec';
import { generateError, generateShortUrlDetailsPage, generateUrlDetails } from 'src/app/utils/testing-mock-service.spec';
import { ListAllUrlsComponent } from './list-all-urls.component';

describe('ListAllUrlsComponent', () => {
  let component: ListAllUrlsComponent;
  let dialog: MatDialog;
  let fixture: ComponentFixture<ListAllUrlsComponent>;
  let spyCallBackendService: jasmine.SpyObj<CallBackendService>;

  let urlDetailsPage = generateShortUrlDetailsPage();

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ListAllUrlsComponent, AddProtocolUrlPipe, SimplifyUrlPipe, MatDialogModule, TranslocoTestRootModule, MatProgressSpinnerModule],
      providers: [
        UrlShortenerStore,
        { provide: CallBackendService, useValue: jasmine.createSpyObj('CallBackendService', ['getAllShortUrlDetails', 'deleteShortUrlDetailsFromId']) },
        provideExperimentalZonelessChangeDetection(),
      ]
    }).compileComponents();

    spyCallBackendService = TestBed.inject(CallBackendService) as jasmine.SpyObj<CallBackendService>;
    spyCallBackendService.getAllShortUrlDetails.and.returnValue(of(urlDetailsPage));
    
    fixture = TestBed.createComponent(ListAllUrlsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    dialog = component['dialog'];
  });

  it('should create', () => {
    // [When] / [Then]
    expect(component).toBeTruthy();
    expect(component.PAGE_SIZE).toEqual(5);
    expect(component.isLoading()).toBeFalse();
    expect(component.serverErrorMsg()).toEqual('');
    expect(component.shortUrlDetailsPage()).toEqual(urlDetailsPage);
    expect(component.EMPTY_RESULT_SET.count).toEqual(20);
    expect(component.EMPTY_RESULT_SET.pageCount).toEqual(4);
    expect(spyCallBackendService.getAllShortUrlDetails.calls.count()).toEqual(1);
    expect(spyCallBackendService.getAllShortUrlDetails.calls.first().args).toEqual([0, 5]);
  });


  it('pageChange() should change page', () => {
    // [When]
    component.pageChange({pageIndex: 2} as PageEvent);

    // [Then]
    expect(component.PAGE_SIZE).toEqual(5);
    expect(component.isLoading()).toBeFalse();
    expect(component.serverErrorMsg()).toEqual('');
    expect(component.shortUrlDetailsPage()).toEqual(urlDetailsPage);
    expect(component.EMPTY_RESULT_SET.count).toEqual(20);
    expect(component.EMPTY_RESULT_SET.pageCount).toEqual(4);
    expect(spyCallBackendService.getAllShortUrlDetails.calls.count()).toEqual(2);
    expect(spyCallBackendService.getAllShortUrlDetails.calls.first().args).toEqual([0, 5]);
    expect(spyCallBackendService.getAllShortUrlDetails.calls.mostRecent().args).toEqual([2, 5]);
  });


  it('pageChange() when already loading should do nothing', () => {
    // [Given]
    component.isLoading.set(true);

    // [When]
    component.pageChange({pageIndex: 1} as PageEvent);

    // [Then]
    expect(component.isLoading()).toBeTrue();
    expect(component.serverErrorMsg()).toEqual('');
    expect(spyCallBackendService.getAllShortUrlDetails.calls.count()).toEqual(1);
  });
  

  it('deleteUrl() should delete a URL', () => {
    // [Given]
    const urlId = '12345abcde';
    spyCallBackendService.deleteShortUrlDetailsFromId.and.returnValue(of(generateUrlDetails()));

    // [When]
    component.deleteUrl(urlId);

    // [Then]
    expect(component.isLoading()).toBeFalse();
    expect(component.serverErrorMsg()).toEqual('');
    expect(spyCallBackendService.deleteShortUrlDetailsFromId.calls.count()).toEqual(1);
    expect(spyCallBackendService.deleteShortUrlDetailsFromId.calls.first().args).toEqual([urlId]);
    expect(spyCallBackendService.getAllShortUrlDetails.calls.count()).toEqual(2);
  });

  it('deleteUrl() with a Back-End error should be handled', () => {
    // [Given]
    const urlId = '12345abcde';
    spyCallBackendService.deleteShortUrlDetailsFromId.and.returnValue(throwError(generateError(500)));

    // [When]
    component.deleteUrl(urlId);

    // [Then]
    expect(component.isLoading()).toBeFalse();
    expect(component.serverErrorMsg()).toEqual('Error with 500 status');
    expect(spyCallBackendService.deleteShortUrlDetailsFromId.calls.count()).toEqual(1);
    expect(spyCallBackendService.deleteShortUrlDetailsFromId.calls.first().args).toEqual([urlId]);
    expect(spyCallBackendService.getAllShortUrlDetails.calls.count()).toEqual(1);
  });

  it('openConfirmationDeleteUrlDialog() should open confirmation dialog - answer yes', () => {
    // [Given]
    const urlId = '12345abcde';
    const spyDeleteUrl = spyOn(component, 'deleteUrl');
    const spyDialogOpen = spyOn(dialog, 'open').and.callThrough();
    spyOn(MatDialogRef.prototype, 'afterClosed').and.returnValue(of('yes'));

    // [When]
    component.openConfirmationDeleteUrlDialog(urlId);

    // [Then]
    expect(spyDialogOpen).toHaveBeenCalledOnceWith(component.callAPIDialog);
    expect(spyDeleteUrl).toHaveBeenCalledOnceWith(urlId);
  });

  it('openConfirmationDeleteUrlDialog() should open confirmation dialog - answer no', () => {
    // [Given]
    const urlId = '12345abcde';
    const spyDeleteUrl = spyOn(component, 'deleteUrl');
    const spyDialogOpen = spyOn(dialog, 'open').and.callThrough();
    spyOn(MatDialogRef.prototype, 'afterClosed').and.returnValue(of('no'));

    // [When]
    component.openConfirmationDeleteUrlDialog(urlId);

    // [Then]
    expect(spyDialogOpen).toHaveBeenCalledOnceWith(component.callAPIDialog);
    expect(spyDeleteUrl).toHaveBeenCalledTimes(0);
  });
});

describe('ListAllUrlsComponent - Back-End in error', () => {
  let component: ListAllUrlsComponent;
  let fixture: ComponentFixture<ListAllUrlsComponent>;
  let spyCallBackendService: jasmine.SpyObj<CallBackendService>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ListAllUrlsComponent, AddProtocolUrlPipe, SimplifyUrlPipe, MatDialogModule, TranslocoTestRootModule, MatProgressSpinnerModule],
      providers: [
        { provide: CallBackendService, useValue: jasmine.createSpyObj('CallBackendService', ['getAllShortUrlDetails']) },
        provideExperimentalZonelessChangeDetection(),
      ]
    }).compileComponents();
  
    spyCallBackendService = TestBed.inject(CallBackendService) as jasmine.SpyObj<CallBackendService>;
    spyCallBackendService.getAllShortUrlDetails.and.returnValue(throwError(generateError(500)));
    
    fixture = TestBed.createComponent(ListAllUrlsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create and show an error message', () => {
    // [When] / [Then]
    expect(component).toBeTruthy();
    expect(component.PAGE_SIZE).toEqual(5);
    expect(component.isLoading()).toBeFalse();
    expect(component.serverErrorMsg()).toEqual('Error with 500 status');
    expect(component.shortUrlDetailsPage()).toEqual(component.EMPTY_RESULT_SET);
    expect(spyCallBackendService.getAllShortUrlDetails.calls.count()).toEqual(1);
    expect(spyCallBackendService.getAllShortUrlDetails.calls.first().args).toEqual([0, 5]);
  });
});