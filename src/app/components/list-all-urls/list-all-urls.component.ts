import { NgClass, NgIf } from '@angular/common';
import { ChangeDetectionStrategy, Component, inject, OnInit, signal, TemplateRef, ViewChild } from '@angular/core';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { MatPaginatorModule, PageEvent } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { EMPTY } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ShortUrlDetailsPage } from 'src/app/models/short-url-details-page.model';
import { AddProtocolUrlPipe } from 'src/app/pipes/add-protocol-url.pipe';
import { SimplifyUrlPipe } from 'src/app/pipes/simplify-url.pipe';
import { CallBackendService } from 'src/app/services/call-backend.service';
import { GoogleAnalyticsService } from 'src/app/services/google-analytics.service';
import { TranslocoRootModule } from 'src/app/transloco/transloco-root.module';


@Component({
    selector: 'app-list-all-urls',
    templateUrl: './list-all-urls.component.html',
    styleUrls: ['./list-all-urls.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [TranslocoRootModule, NgClass, NgIf, MatPaginatorModule, MatProgressSpinnerModule, MatDialogModule, SimplifyUrlPipe, AddProtocolUrlPipe]
})
export class ListAllUrlsComponent implements OnInit {
  private callBackendService = inject(CallBackendService);
  private googleAnalyticsService = inject(GoogleAnalyticsService);
  private dialog = inject(MatDialog);

  @ViewChild('callAPIDialog') callAPIDialog!: TemplateRef<any>;

  PAGE_SIZE = 5;
  EMPTY_RESULT_SET = new ShortUrlDetailsPage(0, this.PAGE_SIZE, 0, 0, []);
  ROW_HEIGHT_EM = 4.6;

  shortUrlDetailsPage = signal(this.EMPTY_RESULT_SET);
  isLoading = signal(false);
  serverErrorMsg = signal('');


  ngOnInit(): void {
    this.googleAnalyticsService.logAdminScreenLoad();
    this.pageChange();
  }

  pageChange(page?: PageEvent): void {
    const newPageIndex = page?.pageIndex || 0;
    if (this.isLoading()) {
      return;
    }
    this.googleAnalyticsService.logAdminScreenPageChange();
    this.isLoading.set(true);
    this.callBackendService
      .getAllShortUrlDetails(newPageIndex, this.PAGE_SIZE)
      .pipe(
        catchError(error => {
          this.serverErrorMsg.set(error.message);
          this.isLoading.set(false);
          this.shortUrlDetailsPage.set(this.EMPTY_RESULT_SET);
          return EMPTY;
        }))
      .subscribe((data) => {
        this.serverErrorMsg.set('');
        this.isLoading.set(false);
        this.shortUrlDetailsPage.set(data);
        this.EMPTY_RESULT_SET.count = data.count;
        this.EMPTY_RESULT_SET.pageCount = data.pageCount;
      });
  }

  openConfirmationDeleteUrlDialog(shortUrlId: string): void {
    const dialogRef = this.dialog.open(this.callAPIDialog);
    dialogRef.afterClosed().subscribe(result => {
      if (result === 'yes') {
        this.deleteUrl(shortUrlId);
      }
    });
  }

  deleteUrl(shortUrlId: string): void {
    this.googleAnalyticsService.logAdminScreenDeleteUrl();
    this.isLoading.set(true);
    this.callBackendService
      .deleteShortUrlDetailsFromId(shortUrlId)
      .pipe(
        catchError(error => {
          this.serverErrorMsg.set(error.message);
          this.isLoading.set(false);
          return EMPTY;
        }))
      .subscribe((_data) => {
        this.isLoading.set(false);
        this.pageChange();
      });
  }
}
