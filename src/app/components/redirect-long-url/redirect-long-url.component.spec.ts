import { LocationStrategy } from '@angular/common';
import { MockLocationStrategy } from '@angular/common/testing';
import { provideExperimentalZonelessChangeDetection } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { provideRouter } from '@angular/router';
import { of, throwError } from 'rxjs';
import { APP_ROUTES } from 'src/app/app.routes';
import { AddProtocolUrlPipe } from 'src/app/pipes/add-protocol-url.pipe';
import { SimplifyUrlPipe } from 'src/app/pipes/simplify-url.pipe';
import { TruncatePipe } from 'src/app/pipes/truncate.pipe';
import { CallBackendService } from 'src/app/services/call-backend.service';
import { GoogleAnalyticsService } from 'src/app/services/google-analytics.service';
import { UrlShortenerStore } from 'src/app/store/url-shortener.store';
import { TranslocoTestRootModule } from 'src/app/transloco/transloco-testing.module.spec';
import { generateError, generateUrlDetails } from 'src/app/utils/testing-mock-service.spec';
import { UrlDetails } from 'src/generated/models';
import { RedirectLongUrlComponent } from './redirect-long-url.component';


describe('RedirectLongUrlComponent', () => {
  let component: RedirectLongUrlComponent;
  let fixture: ComponentFixture<RedirectLongUrlComponent>;
  let spyCallBackendService: jasmine.SpyObj<CallBackendService>;
  let spyGoogleAnalyticsService: jasmine.SpyObj<GoogleAnalyticsService>;

  let shortUrlDetails: UrlDetails;

  let mockWindow = { location: { href: '' } } as Window & typeof globalThis;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RedirectLongUrlComponent, AddProtocolUrlPipe, TruncatePipe, SimplifyUrlPipe, TranslocoTestRootModule,
      ],
      providers: [
        UrlShortenerStore,
        AddProtocolUrlPipe,
        { provide: LocationStrategy, useClass: MockLocationStrategy },
        { provide: CallBackendService, useValue: jasmine.createSpyObj('CallBackendService', ['getShortUrlDetailsFromId']) },
        { provide: GoogleAnalyticsService, useValue: jasmine.createSpyObj('GoogleAnalyticsService', ['logRedirectionScreenLoad', 'logRedirectionScreenRedirectToUrl']) },
        provideRouter(APP_ROUTES),
        provideExperimentalZonelessChangeDetection(),
      ]
    }).compileComponents();
      
    spyCallBackendService = TestBed.inject(CallBackendService) as jasmine.SpyObj<CallBackendService>;
    spyGoogleAnalyticsService = TestBed.inject(GoogleAnalyticsService) as jasmine.SpyObj<GoogleAnalyticsService>;
    shortUrlDetails = generateUrlDetails();
    shortUrlDetails.longUrl = 'https://www.something.com';
    spyCallBackendService.getShortUrlDetailsFromId.withArgs('').and.returnValue(of(shortUrlDetails));
    
    fixture = TestBed.createComponent(RedirectLongUrlComponent);
    component = fixture.componentInstance;
    component['window'] = mockWindow;
    mockWindow.location.href = '';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.shortUrlDetails()).toEqual(shortUrlDetails);
    expect(component.serverErrorMsg()).toEqual('');
    expect(mockWindow.location.href).toBe('');
    expect(spyGoogleAnalyticsService.logRedirectionScreenLoad).toHaveBeenCalled();
  });
  
  
  it('should redirect to original URL after 3 seconds', (done) => {
    // [When]
    new Promise((resolve) => setTimeout(resolve, 4500)).then(() => {
      // [Then]
      expect(mockWindow.location.href).toBe(shortUrlDetails.longUrl);
      expect(spyGoogleAnalyticsService.logRedirectionScreenRedirectToUrl).toHaveBeenCalled();
      done();
    });
  });
  
});


describe('RedirectLongUrlComponent - Back-End in error', () => {
  let component: RedirectLongUrlComponent;
  let fixture: ComponentFixture<RedirectLongUrlComponent>;
  let spyCallBackendService: jasmine.SpyObj<CallBackendService>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RedirectLongUrlComponent, AddProtocolUrlPipe, TruncatePipe, SimplifyUrlPipe, TranslocoTestRootModule,
      ],
      providers: [
        AddProtocolUrlPipe,
        { provide: LocationStrategy, useClass: MockLocationStrategy },
        { provide: CallBackendService, useValue: jasmine.createSpyObj('CallBackendService', ['getShortUrlDetailsFromId']) },
        provideRouter(APP_ROUTES),
        provideExperimentalZonelessChangeDetection(),
      ]
    })
      .compileComponents();
  
    spyCallBackendService = TestBed.inject(CallBackendService) as jasmine.SpyObj<CallBackendService>;
    spyCallBackendService.getShortUrlDetailsFromId.and.returnValue(throwError(() => generateError(500)));
    
    fixture = TestBed.createComponent(RedirectLongUrlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create and show an error', () => {
    expect(component).toBeTruthy();
    expect(component.shortUrlDetails()).toBeUndefined();
    expect(component.serverErrorMsg()).toEqual('Error with 500 status');
  });
});