import { ChangeDetectionStrategy, Component, inject, OnDestroy, OnInit, signal, WritableSignal } from '@angular/core';
import { Router } from '@angular/router';
import { EMPTY } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AddProtocolUrlPipe } from 'src/app/pipes/add-protocol-url.pipe';
import { SimplifyUrlPipe } from 'src/app/pipes/simplify-url.pipe';
import { TruncatePipe } from 'src/app/pipes/truncate.pipe';
import { CallBackendService } from 'src/app/services/call-backend.service';
import { GoogleAnalyticsService } from 'src/app/services/google-analytics.service';
import { TranslocoRootModule } from 'src/app/transloco/transloco-root.module';
import { UrlDetails } from 'src/generated/models';

@Component({
    selector: 'app-redirect-long-url',
    templateUrl: './redirect-long-url.component.html',
    styleUrls: ['./redirect-long-url.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [TranslocoRootModule, SimplifyUrlPipe, AddProtocolUrlPipe, TruncatePipe]
})
export class RedirectLongUrlComponent implements OnInit, OnDestroy {

  private window = window;
  private callBackendService = inject(CallBackendService);
  private googleAnalyticsService = inject(GoogleAnalyticsService);
  private router = inject(Router);
  private addProtocolUrlPipe = inject(AddProtocolUrlPipe);

  shortUrlDetails: WritableSignal<UrlDetails | undefined> = signal(undefined);
  secondsLeft = signal(3);
  serverErrorMsg = signal('');
  timer?: any;


  ngOnInit(): void {
    this.googleAnalyticsService.logRedirectionScreenLoad();
    this.callBackendService
    .getShortUrlDetailsFromId(this.getShortUrlIdInUrl())
    .pipe(
      catchError(error => {
        this.serverErrorMsg.set(error.message);
        return EMPTY;
      }))
    .subscribe((data: UrlDetails) => {
      this.shortUrlDetails.set(data);
      this.startTimer(data);
    });
  }

  ngOnDestroy(): void {
    clearInterval(this.timer);
  }


  startTimer(data: UrlDetails): void {
    this.timer = setInterval(() => {
      if (this.secondsLeft() > 0) {
        this.secondsLeft.update((v) => v - 1);
      }
      else {
       this.redirectToLongUrl(data.longUrl);
      }
    }, 1000);
  }

  getShortUrlIdInUrl(): string {
    return this.router.url.replace('/', '');
  }

  redirectToLongUrl(longUrl: string): void {
    this.googleAnalyticsService.logRedirectionScreenRedirectToUrl();
    this.window.location.href = this.addProtocolUrlPipe.transform(longUrl);
  }
}
