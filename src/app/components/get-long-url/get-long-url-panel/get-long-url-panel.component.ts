import { ChangeDetectionStrategy, Component } from '@angular/core';
import { UrlDetails } from 'src/generated/models';
import { GetLongUrlFormComponent } from '../get-long-url-form/get-long-url-form.component';
import { GetLongUrlResponseComponent } from '../get-long-url-response/get-long-url-response.component';

@Component({
    selector: 'app-get-long-url-panel',
    templateUrl: './get-long-url-panel.component.html',
    styleUrls: ['./get-long-url-panel.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [GetLongUrlFormComponent, GetLongUrlResponseComponent]
})
export class GetLongUrlPanelComponent {

  shortUrlDetailsObtained?: UrlDetails;

  public onFormSubmitted(shortUrlDetailsObtained: UrlDetails): void {
    this.shortUrlDetailsObtained = shortUrlDetailsObtained;
  }

  public onFindAnotherUrl(): void {
    this.shortUrlDetailsObtained = undefined;
  }
}
