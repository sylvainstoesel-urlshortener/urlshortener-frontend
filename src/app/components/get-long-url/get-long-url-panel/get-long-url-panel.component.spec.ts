import { provideHttpClient } from '@angular/common/http';
import { provideExperimentalZonelessChangeDetection } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SimplifyUrlPipe } from 'src/app/pipes/simplify-url.pipe';
import { TranslocoTestRootModule } from 'src/app/transloco/transloco-testing.module.spec';
import { generateUrlDetails } from 'src/app/utils/testing-mock-service.spec';
import { GetLongUrlPanelComponent } from './get-long-url-panel.component';

describe('GetLongUrlPanelComponent', () => {
  let component: GetLongUrlPanelComponent;
  let fixture: ComponentFixture<GetLongUrlPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [GetLongUrlPanelComponent, TranslocoTestRootModule],
      providers: [
        SimplifyUrlPipe,
        provideHttpClient(),
        provideExperimentalZonelessChangeDetection()
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GetLongUrlPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('onFormSubmitted() should update the found short URL details', () => {
    // [Given]
    const shortUrlDetailsObtained = generateUrlDetails();
    // [When]
    component.onFormSubmitted(shortUrlDetailsObtained);
    // [Then]
    expect(component.shortUrlDetailsObtained).toEqual(shortUrlDetailsObtained);
  });

  it('onFindAnotherUrl() should reset the found short URL details', () => {
    // [Given]
    component.shortUrlDetailsObtained = generateUrlDetails();
    // [When]
    component.onFindAnotherUrl();
    // [Then]
    expect(component.shortUrlDetailsObtained).toBeUndefined();
  });
});
