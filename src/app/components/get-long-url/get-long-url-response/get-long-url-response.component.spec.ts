import { Clipboard } from '@angular/cdk/clipboard';
import { provideExperimentalZonelessChangeDetection } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { TranslocoService } from '@jsverse/transloco';
import { UrlShortenerStore } from 'src/app/store/url-shortener.store';
import { TranslocoTestRootModule } from 'src/app/transloco/transloco-testing.module.spec';
import { generateUrlDetails } from 'src/app/utils/testing-mock-service.spec';
import { GetLongUrlResponseComponent } from './get-long-url-response.component';

describe('GetLongUrlResponseComponent', () => {
  let component: GetLongUrlResponseComponent;
  let fixture: ComponentFixture<GetLongUrlResponseComponent>;

  let spyTranslocoServiceTranslate: jasmine.Spy;
  let spyEventEmitterEmit: jasmine.Spy;
  let spyMatSnackBar: jasmine.SpyObj<MatSnackBar>;
  let spyClipboard: jasmine.SpyObj<Clipboard>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [GetLongUrlResponseComponent, MatSnackBarModule, TranslocoTestRootModule],
      providers: [
        UrlShortenerStore,
        { provide: MatSnackBar, useValue: jasmine.createSpyObj('MatSnackBar', ['open']) },
        { provide: Clipboard, useValue: jasmine.createSpyObj('Clipboard', ['copy']) },
        provideExperimentalZonelessChangeDetection(),
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GetLongUrlResponseComponent);
    
    spyMatSnackBar = TestBed.inject(MatSnackBar) as jasmine.SpyObj<MatSnackBar>;
    spyClipboard = TestBed.inject(Clipboard) as jasmine.SpyObj<Clipboard>;

    const translocoService = TestBed.inject(TranslocoService) as TranslocoService;
    spyTranslocoServiceTranslate = spyOn(translocoService, 'translate');
    
    component = fixture.componentInstance;
    fixture.detectChanges();

    spyEventEmitterEmit = spyOn(component.whenFindAnotherUrl, 'emit');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('copyToClipboard() should not copy short URL to clipboard if not present', () => {
    // [Given]
    component.shortUrlDetailsObtained = undefined;
    // [When]
    component.copyToClipboard();
    // [Then]
    expect(spyClipboard.copy.calls.count()).toEqual(0);
    expect(spyTranslocoServiceTranslate.calls.count()).toEqual(0);
    expect(spyMatSnackBar.open.calls.count()).toEqual(0);

  });

  it('copyToClipboard() should copy long URL to clipboard when present', () => {
    // [Given]
    const translation = 'Copied!';
    spyTranslocoServiceTranslate.and.returnValue(translation);
    component.shortUrlDetailsObtained = generateUrlDetails();

    // [When]
    component.copyToClipboard();

    // [Then]
    expect(spyClipboard.copy.calls.count()).toEqual(1);
    expect(spyClipboard.copy.calls.first().args).toEqual([component.shortUrlDetailsObtained.longUrl]);
    expect(spyTranslocoServiceTranslate.calls.count()).toEqual(1);
    expect(spyTranslocoServiceTranslate.calls.first().args).toEqual(['get-url.response.copied-clipboard']);
    expect(spyMatSnackBar.open.calls.count()).toEqual(1);
    expect(spyMatSnackBar.open.calls.first().args).toEqual([translation, undefined, {
      horizontalPosition: 'right',
      verticalPosition: 'bottom',
      duration: 2000
    }]);

  });

  it('findAnotherUrl() should emit an event', () => {
    // [When]
    component.findAnotherUrl();
    // [Then]
    expect(spyEventEmitterEmit.calls.count()).toEqual(1);
  });
});
