import { Clipboard } from '@angular/cdk/clipboard';
import { ChangeDetectionStrategy, Component, EventEmitter, inject, Input, Output } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TranslocoService } from '@jsverse/transloco';
import { AddProtocolUrlPipe } from 'src/app/pipes/add-protocol-url.pipe';
import { TranslocoRootModule } from 'src/app/transloco/transloco-root.module';
import { UrlDetails } from 'src/generated/models';

@Component({
    selector: 'app-get-long-url-response',
    templateUrl: './get-long-url-response.component.html',
    styleUrls: ['./get-long-url-response.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [TranslocoRootModule, AddProtocolUrlPipe]
})
export class GetLongUrlResponseComponent {
  private snackBar = inject(MatSnackBar);
  private translate = inject(TranslocoService);
  private clipboard = inject(Clipboard);

  @Input() shortUrlDetailsObtained?: UrlDetails;
  // To be sent to parent component (panel)
  @Output() whenFindAnotherUrl = new EventEmitter<any>();

  copyToClipboard(): void {
    if (this.shortUrlDetailsObtained) {
      this.clipboard.copy(this.shortUrlDetailsObtained.longUrl);
      this.snackBar.open(this.translate.translate('get-url.response.copied-clipboard'), undefined, {
        horizontalPosition: 'right',
        verticalPosition: 'bottom',
        duration: 2000
      });
    }
  }

  findAnotherUrl(): void {
    this.whenFindAnotherUrl.emit();
  }
}
