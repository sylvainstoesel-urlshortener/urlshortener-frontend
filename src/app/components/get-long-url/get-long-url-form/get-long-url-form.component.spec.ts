import { provideExperimentalZonelessChangeDetection, signal } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { deepComputed } from '@ngrx/signals';
import { of, throwError } from 'rxjs';
import { SimplifyUrlPipe } from 'src/app/pipes/simplify-url.pipe';
import { CallBackendService } from 'src/app/services/call-backend.service';
import { UrlShortenerStore } from 'src/app/store/url-shortener.store';
import { TranslocoTestRootModule } from 'src/app/transloco/transloco-testing.module.spec';
import { generateApiUrlParameters, generateError, generateUrlDetails } from 'src/app/utils/testing-mock-service.spec';
import { GetLongUrlFormComponent } from './get-long-url-form.component';

describe('GetLongUrlFormComponent', () => {
  let component: GetLongUrlFormComponent;
  let fixture: ComponentFixture<GetLongUrlFormComponent>;
  let spyCallBackendService: jasmine.SpyObj<CallBackendService>;
  let spyEventEmitterEmit: jasmine.Spy;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [GetLongUrlFormComponent, SimplifyUrlPipe, TranslocoTestRootModule, MatProgressSpinnerModule, FormsModule, ReactiveFormsModule],
      providers: [
        SimplifyUrlPipe,
        {
          provide: CallBackendService,
          useValue: jasmine.createSpyObj('CallBackendService', ['getLongUrlValue']),
        },
        {
          provide: UrlShortenerStore,
          useValue: {
            urlParameters: deepComputed(signal(generateApiUrlParameters()))
          }
        },
        provideExperimentalZonelessChangeDetection()
      ],
    }).compileComponents();

    spyCallBackendService = TestBed.inject(
      CallBackendService
    ) as jasmine.SpyObj<CallBackendService>;

    fixture = TestBed.createComponent(GetLongUrlFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    spyEventEmitterEmit = spyOn(component.whenFormSubmitted, 'emit');
  });


  it('should create', () => {
    // [When] / [Then]
    expect(component).toBeTruthy();
    expect(component.apiParameters()).toEqual(generateApiUrlParameters());
    expect(component.getLongUrlForm).toBeTruthy();
    expect(component.getLongUrlForm.controls.shortUrlValue).toBeTruthy();
    expect(component.isSubmitted()).toBeFalse();
    expect(component.isLoading()).toBeFalse();
    expect(component.serverErrorMsg()).toEqual('');
    expect(component.shortUrlPlaceholder()).toEqual('short.ca/∗∗∗∗∗∗∗∗∗∗');
    expect(component.longUrlRegex()).toEqual('^(https?://)?short\\.ca/[a-zA-Z0-9]{10}$');
    expect(component.shortUrlValue.errors?.required).toBeTruthy();
    expect(component.shortUrlValue.errors?.pattern).toBeFalsy();
    expect(component.shortUrlValue.hasValidator(Validators.required)).toBeTrue();
  });


  describe('onSubmit()', () => {
    it('should submit the form when short URL is valid', () => {
      // [Given]
      const urlDetailsReturned = generateUrlDetails();
      component.getLongUrlForm.controls.shortUrlValue.setValue(
        'short.ca/1234567890'
      );
      spyCallBackendService.getLongUrlValue.and.returnValue(
        of(urlDetailsReturned)
      );
      // [When]
      component.onSubmit();
      // [Then]
      expect(component.shortUrlValue.errors).toBeFalsy();
      expect(component.isSubmitted()).toBeFalse();
      expect(component.isLoading()).toBeFalse();
      expect(component.showIsValid).toBeFalse();
      expect(spyCallBackendService.getLongUrlValue.calls.count()).toEqual(1);
      expect(spyCallBackendService.getLongUrlValue.calls.first().args).toEqual([
        'short.ca/1234567890',
      ]);
      expect(spyEventEmitterEmit.calls.count()).toEqual(1);
      expect(spyEventEmitterEmit.calls.first().args).toEqual([
        urlDetailsReturned,
      ]);
    });
  
    it('should submit the form with valid short URL and return an error when API responds an error', () => {
      // [Given]
      const error = generateError(500);
      component.getLongUrlForm.controls.shortUrlValue.setValue(
        'short.ca/1234567890'
      );
      spyCallBackendService.getLongUrlValue.and.returnValue(throwError(error));
      // [When]
      component.onSubmit();
      // [Then]
      expect(component.shortUrlValue.errors).toBeFalsy();
      expect(component.isSubmitted()).toBeTrue();
      expect(component.isLoading()).toBeFalse();
      expect(component.showIsValid).toBeTrue();
      expect(component.serverErrorMsg()).toEqual(error.message);
      expect(spyCallBackendService.getLongUrlValue.calls.count()).toEqual(1);
      expect(spyCallBackendService.getLongUrlValue.calls.first().args).toEqual([
        'short.ca/1234567890',
      ]);
      expect(spyEventEmitterEmit.calls.count()).toEqual(0);
    });
  
    it('should not submit the form with empty short URL', () => {
      // [When]
      component.onSubmit();
      // [Then]
      expect(component.shortUrlValue.errors?.required).toBeTruthy();
      expect(component.shortUrlValue.errors?.pattern).toBeFalsy();
      expect(component.isSubmitted()).toBeTrue();
      expect(component.isLoading()).toBeFalse();
      expect(component.showIsValid).toBeTrue();
      expect(spyCallBackendService.getLongUrlValue.calls.count()).toEqual(0);
    });
  
    it('should not submit the form with invalid short URL', () => {
      // [Given]
      component.getLongUrlForm.controls.shortUrlValue.setValue('shortca');
      // [When]
      component.onSubmit();
      // [Then]
      expect(component.shortUrlValue.errors?.required).toBeFalsy();
      expect(component.shortUrlValue.errors?.pattern).toBeTruthy();
      expect(component.isSubmitted()).toBeTrue();
      expect(component.isLoading()).toBeFalse();
      expect(component.showIsValid).toBeTrue();
      expect(spyCallBackendService.getLongUrlValue.calls.count()).toEqual(0);
    });
  })
  

  describe('buildShortUrlValue()', () => {
    it('should build URL accordingly to apiParameters (no protocol)', () => {
      // [Given] / [When]
      const result = component.buildShortUrlValue('http://short.ca/1234567890');
      // [Then]
      expect(result).toEqual('short.ca/1234567890');
    });
    
    it('should build URL accordingly to apiParameters (http)', () => {
      // [Given]
      component.apiParameters = deepComputed(signal({shortUrlIdSize: 10, shortUrlDomain: 'http://short.ca/'}));
      // [When]
      const result = component.buildShortUrlValue('https://short.ca/1234567890');
      // [Then]
      expect(result).toEqual('http://short.ca/1234567890');
    });
    
    it('should build URL accordingly to apiParameters (https)', () => {
      // [Given]
      component.apiParameters = deepComputed(signal({shortUrlIdSize: 10, shortUrlDomain: 'https://short.ca/'}));
      // [When]
      const result = component.buildShortUrlValue('http://short.ca/1234567890');
      // [Then]
      expect(result).toEqual('https://short.ca/1234567890');
    });
  });


  describe('onShortUrlValueChanged()', () => {
    it('should reset isSubmitted and serverErrorMsg values', () => {
      // [Given]
      component.isSubmitted.set(true);
      component.serverErrorMsg.set('Error');
      // [When]
      component.onShortUrlValueChanged();
      // [Then]
      expect(component.isSubmitted()).toBeFalse();
      expect(component.isLoading()).toBeFalse();
    });
  });


  describe('shortUrlValue (getter)', () => {
    it('should return the form control shortUrlValue', () => {
      // [When] / [Then]
      expect(component.shortUrlValue).toEqual(
        component.getLongUrlForm.controls.shortUrlValue
      );
    });
  });


  describe('showValidationErrors (getter)', () => {
    it('should return false by default', () => {
      // [When] / [Then]
      expect(component.showValidationErrors).toBeFalse();
    });

    it('should return true when form is submitted', () => {
      // [Given]
      component.getLongUrlForm.controls.shortUrlValue.setValue(
        'short.ca/1234567890'
      );
      component.isSubmitted.set(true);
      component.serverErrorMsg.set('Error');
      // [When] / [Then]
      expect(component.showValidationErrors).toBeTrue();
    });
  });


  describe('showIsValid (getter)', () => {
    it('should return false by default', () => {
      // [When] / [Then]
      expect(component.showIsValid).toBeFalse();
    });
  });
});
