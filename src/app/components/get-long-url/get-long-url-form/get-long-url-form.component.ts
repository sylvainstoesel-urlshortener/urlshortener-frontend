import { NgClass, NgIf } from '@angular/common';
import { ChangeDetectionStrategy, Component, computed, effect, EventEmitter, inject, Output, signal } from '@angular/core';
import { FormControl, NonNullableFormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { EMPTY } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { SimplifyUrlPipe } from 'src/app/pipes/simplify-url.pipe';
import { CallBackendService } from 'src/app/services/call-backend.service';
import { GoogleAnalyticsService } from 'src/app/services/google-analytics.service';
import { UrlShortenerStore } from 'src/app/store/url-shortener.store';
import { TranslocoRootModule } from 'src/app/transloco/transloco-root.module';

@Component({
    selector: 'app-get-long-url-form',
    templateUrl: './get-long-url-form.component.html',
    styleUrls: ['./get-long-url-form.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [TranslocoRootModule, NgClass, NgIf, MatProgressSpinnerModule, ReactiveFormsModule]
})
export class GetLongUrlFormComponent {
  private store = inject(UrlShortenerStore);
  private formBuilder = inject(NonNullableFormBuilder);
  private callBackendService = inject(CallBackendService);
  private googleAnalyticsService = inject(GoogleAnalyticsService);
  private simplifyUrlPipe = inject(SimplifyUrlPipe);

  // To be sent to parent component (panel) when the form is submitted.
  @Output() whenFormSubmitted = new EventEmitter<any>();

  // The form.
  getLongUrlForm = this.formBuilder.group({
    shortUrlValue: this.formBuilder.control('', {
      validators: [Validators.required]
    }),
  });
  
  // Parameters retrieved from the store (global state of the app).
  apiParameters = this.store.urlParameters;
  
  // The other values used by this form and this component.
  shortUrlPlaceholder = computed(() => this.apiParameters().shortUrlDomain + '∗'.repeat(this.apiParameters().shortUrlIdSize));
  longUrlRegex = computed(() => '^(https?://)?'
        + this.escapeShortUrlDomainForRegExp(this.apiParameters().shortUrlDomain)
        + '[a-zA-Z0-9]{' + this.apiParameters().shortUrlIdSize + '}$');
  isSubmitted = signal(false);
  isLoading = signal(false);
  serverErrorMsg = signal('');

  constructor() {
    effect(() => {
      // Update the validators of the form if the URL Api parameters change.
      this.shortUrlValue.setValidators([Validators.required, Validators.pattern(new RegExp(this.longUrlRegex(), 'i'))]);
    });
  }

  onShortUrlValueChanged(): void {
    this.isSubmitted.set(false);
    this.serverErrorMsg.set('');
  }

  onSubmit(): void {
    if (this.getLongUrlForm.invalid || !this.apiParameters || this.isSubmitted()) {
      this.isSubmitted.set(true);
      return;
    }
    this.googleAnalyticsService.logMainScreenRetrieveUrl();
    this.isSubmitted.set(true);
    this.isLoading.set(true);

    this.callBackendService
      .getLongUrlValue(this.buildShortUrlValue(this.getLongUrlForm.getRawValue().shortUrlValue))
      .pipe(
        catchError(error => {
          this.serverErrorMsg.set(error.message);
          this.isLoading.set(false);
          return EMPTY;
        }))
      .subscribe((data) => {
        this.isSubmitted.set(false);
        this.isLoading.set(false);
        this.whenFormSubmitted.emit(data);
      });
  }

  buildShortUrlValue(shortUrlValue: string): string {
    shortUrlValue = this.simplifyUrlPipe.transform(shortUrlValue);
    if (this.apiParameters().shortUrlDomain.startsWith('https://')) {
      shortUrlValue = 'https://' + shortUrlValue;
    }
    else if (this.apiParameters().shortUrlDomain.startsWith('http://')) {
      shortUrlValue = 'http://' + shortUrlValue;
    }
    return shortUrlValue;
  }

  private escapeShortUrlDomainForRegExp(input: string): string {
    return this.simplifyUrlPipe.transform(input).replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
  }

  // Getters
  get shortUrlValue(): FormControl<string> {
    return this.getLongUrlForm.controls.shortUrlValue;
  }

  get showValidationErrors(): boolean {
    return (!!this.shortUrlValue.errors || !!this.serverErrorMsg()) && this.showIsValid;
  }

  get showIsValid(): boolean {
    return this.shortUrlValue.touched || this.isSubmitted();
  }
}
