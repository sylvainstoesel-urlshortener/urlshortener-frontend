import { ChangeDetectionStrategy, Component, signal } from '@angular/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { TranslocoRootModule } from 'src/app/transloco/transloco-root.module';
import { CreateShortUrlPanelComponent } from '../create-short-url/create-short-url-panel/create-short-url-panel.component';
import { GetLongUrlPanelComponent } from '../get-long-url/get-long-url-panel/get-long-url-panel.component';

@Component({
    selector: 'app-accordion',
    templateUrl: './accordion.component.html',
    styleUrls: ['./accordion.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [TranslocoRootModule, MatExpansionModule, CreateShortUrlPanelComponent, GetLongUrlPanelComponent]
})
export class AccordionComponent {
  createUrlPanelOpen = signal(false);
  getUrlPanelOpen = signal(false);
}
