import { provideHttpClient } from '@angular/common/http';
import { provideExperimentalZonelessChangeDetection } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatExpansionModule } from '@angular/material/expansion';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SimplifyUrlPipe } from 'src/app/pipes/simplify-url.pipe';
import { UrlShortenerStore } from 'src/app/store/url-shortener.store';
import { TranslocoTestRootModule } from 'src/app/transloco/transloco-testing.module.spec';
import { AccordionComponent } from './accordion.component';

describe('AccordionComponent', () => {
  let component: AccordionComponent;
  let fixture: ComponentFixture<AccordionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MatExpansionModule, BrowserAnimationsModule, AccordionComponent, TranslocoTestRootModule],
      providers: [
        UrlShortenerStore,
        SimplifyUrlPipe,
        provideHttpClient(),
        provideExperimentalZonelessChangeDetection(),
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(AccordionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.createUrlPanelOpen()).toBeFalse();
    expect(component.getUrlPanelOpen()).toBeFalse();
  });
});
