import { provideHttpClient } from '@angular/common/http';
import { provideExperimentalZonelessChangeDetection, signal } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatMenuModule } from '@angular/material/menu';
import { provideRouter, RouterModule } from '@angular/router';
import { APP_ROUTES } from 'src/app/app.routes';
import { SimplifyUrlPipe } from 'src/app/pipes/simplify-url.pipe';
import { LanguageSelectService } from 'src/app/services/language-select.service';
import { UrlShortenerStore } from 'src/app/store/url-shortener.store';
import { TranslocoTestRootModule } from 'src/app/transloco/transloco-testing.module.spec';
import { FooterComponent } from './footer.component';

describe('FooterComponent', () => {
  let store: any;
  let component: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;

  beforeEach(async () => {
    LanguageSelectService;
    await TestBed.configureTestingModule({
      imports: [FooterComponent, SimplifyUrlPipe, MatMenuModule, TranslocoTestRootModule, RouterModule],
      providers: [
        provideRouter(APP_ROUTES),
        provideHttpClient(),
        provideExperimentalZonelessChangeDetection(),
        {
          provide: UrlShortenerStore,
          useValue: {
            apiVersion: signal('1.0.123'),
            language: signal('es'),
            changeLanguage: (_language: string) => { },
          },
         },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(FooterComponent);
    store = TestBed.inject(UrlShortenerStore);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('apiVersion() should return the Backend version', () => {
    // [When] / [Then]
    expect(component.apiVersion()).toEqual('1.0.123');
  });

  it('spaVersion() should return the Frontend version', () => {
    // [When] / [Then]
    expect(component.spaVersion()).toEqual('1.9.0');
  });

  it('currentLanguage() should return the current language of the app', () => {
    // [When] / [Then]
    expect(component.currentLanguage()).toEqual('es');
  });

  it('otherLanguages() should return all the languages except the current language', () => {
    // [When] / [Then]
    expect(component.otherLanguages()).toEqual(['en', 'fr', 'it', 'de']);
  });

  it('changeLanguage() should call the store to change language', () => {
    // [Given]
    const spyStore = spyOn(store, 'changeLanguage').and.callFake(() => {});
    // [When]
    component.changeLanguage('fr');
    // [Then]
    expect(spyStore.calls.count()).toEqual(1);
    expect(spyStore.calls.first().args[0]).toEqual('fr');
  });
});