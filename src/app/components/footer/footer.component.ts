import { ChangeDetectionStrategy, Component, computed, inject, signal } from '@angular/core';
import { MatMenuModule } from '@angular/material/menu';
import { RouterModule } from '@angular/router';
import { UrlShortenerStore } from 'src/app/store/url-shortener.store';
import { AVAILABLE_LANGUAGES, TranslocoRootModule } from 'src/app/transloco/transloco-root.module';
import { environment } from 'src/environments/environment';
import packageJson from '../../../../package.json';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [TranslocoRootModule, MatMenuModule, RouterModule]
})
export class FooterComponent {
  private store = inject(UrlShortenerStore);

  BASE_URL = environment.backendUrl;

  currentLanguage = this.store.language;
  otherLanguages = computed(() => AVAILABLE_LANGUAGES.filter((l) => l !== this.currentLanguage()));

  spaVersion = signal(packageJson.version);
  apiVersion = this.store.apiVersion;

  changeLanguage(language: string): void {
    this.store.changeLanguage(language);
  }
}
