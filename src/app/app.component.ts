import { Component, inject, OnInit } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FooterComponent } from './components/footer/footer.component';
import { LoadingComponent } from './components/loading/loading.component';
import { GoogleAnalyticsService } from './services/google-analytics.service';
import { UrlShortenerStore } from './store/url-shortener.store';
import { TranslocoRootModule } from './transloco/transloco-root.module';

@Component({
    imports: [LoadingComponent, TranslocoRootModule, RouterModule, FooterComponent],
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppMainComponent implements OnInit {
  private store = inject(UrlShortenerStore);
  private googleAnalyticsService = inject(GoogleAnalyticsService);

  isLoading = this.store.isLoading;
  serverErrorMsg = this.store.apiErrorMessage;

  ngOnInit(): void {
    this.store.loadMainPage();
    this.googleAnalyticsService.logMainScreenLoad();
  }
}
