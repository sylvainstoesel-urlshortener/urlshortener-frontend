import { UrlDetails } from '../../generated/models';

export class ShortUrlDetailsPage {
    page: number;
    size: number;
    count: number;
    pageCount: number;
    elements: UrlDetails[];
  
    constructor(page: number, size: number, count: number, pageCount: number, elements: UrlDetails[]) {
      this.page = page;
      this.size = size;
      this.count = count;
      this.pageCount = pageCount;
      this.elements = elements;
    }
  }
  