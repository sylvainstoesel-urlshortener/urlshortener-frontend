import { HTTP_INTERCEPTORS, HttpHeaders, provideHttpClient, withInterceptorsFromDi } from '@angular/common/http';
import { HttpTestingController, provideHttpClientTesting } from '@angular/common/http/testing';
import { provideExperimentalZonelessChangeDetection } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { Observable } from 'rxjs';
import { UrlDetailsService, UrlParametersService, UrlSearchService } from 'src/generated/services';
import { UrlDetails, UrlParameters } from '../../generated/models';
import { CacheGetRequestsInterceptor } from '../interceptors/cache-get-requests.interceptor';
import { CatchRequestsErrorsInterceptor } from '../interceptors/catch-requests-errors.interceptor';
import { ShortUrlDetailsPage } from '../models/short-url-details-page.model';
import { TranslocoTestRootModule } from '../transloco/transloco-testing.module.spec';
import { CallBackendService } from './call-backend.service';
import { LanguageSelectService } from './language-select.service';


describe('CallBackendService', () => {
  let service: CallBackendService;
  let httpTestingController: HttpTestingController;
  let basePath = 'https://localhost';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TranslocoTestRootModule],
      providers: [
          CallBackendService,
          {
              provide: HTTP_INTERCEPTORS,
              useClass: CacheGetRequestsInterceptor,
              multi: true,
          },
          {
              provide: HTTP_INTERCEPTORS,
              useClass: CatchRequestsErrorsInterceptor,
              multi: true,
          },
          { provide: LanguageSelectService, useValue: jasmine.createSpyObj('LanguageSelectService', ['getCurrentLanguage']) },
          provideHttpClient(withInterceptorsFromDi()),
          provideHttpClientTesting(),
          provideExperimentalZonelessChangeDetection(),
      ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(CallBackendService);
        
    const urlParametersService = TestBed.inject(UrlParametersService);
    urlParametersService.rootUrl = basePath;
    const urlDetailsService = TestBed.inject(UrlDetailsService);
    urlDetailsService.rootUrl = basePath;
    const urlSearchService = TestBed.inject(UrlSearchService);
    urlSearchService.rootUrl = basePath;
  });

    
  afterEach(() => {
    httpTestingController.verify();
  });


  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('loadApiUrlParameters() should retrieve global API parameters', () => {
    // [Given]
    const dataToBeReturned = { shortUrlIdSize: 10, shortUrlDomain: 'domain.ca' };

    // [When]
    const urldetails: Observable<UrlParameters> = service.getApiUrlParameters();

    // [Then]
    urldetails.subscribe(data => expect(data).toEqual(dataToBeReturned));
    const requestDone = httpTestingController.expectOne(basePath + '/urlparameters');
    expect(requestDone.request.method).toEqual('GET');
    requestDone.flush(dataToBeReturned);
  });


  it('createShortUrlFromLongUrl() should create and retrieve Short Url details', () => {
    // [Given]
    const dataToBeReturned = {
      shortUrlId: '1a2b3c4d5e', shortUrl: 'domain.ca/1a2b3c4d5e',
      longUrl: 'http://long-url.ca/', creationTimestamp: ''
    };

    // [When]
    const urldetails: Observable<UrlDetails> = service.createShortUrlFromLongUrl(dataToBeReturned.longUrl);

    // [Then]
    urldetails.subscribe(data => expect(data).toEqual(dataToBeReturned));
    const requestDone = httpTestingController.expectOne(basePath + '/urldetails');
    expect(requestDone.request.method).toEqual('POST');
    expect(requestDone.request.body).toEqual({ value: dataToBeReturned.longUrl });
    requestDone.flush(dataToBeReturned, { status: 201, statusText: 'Created' });
  });


  it('getShortUrlDetailsFromId() should retrieve short URL Details', () => {
    // [Given]
    const urlDetailsId = '1a2b3c4d5e';
    const dataToBeReturned = {
      shortUrlId: urlDetailsId, shortUrl: 'domain.ca/' + urlDetailsId,
      longUrl: 'http://long-url.ca/', creationTimestamp: ''
    };

    // [When]
    const urldetails: Observable<UrlDetails> = service.getShortUrlDetailsFromId(urlDetailsId);

    // [Then]
    urldetails.subscribe(data => expect(data).toEqual(dataToBeReturned));
    const requestsDone = httpTestingController.expectOne(basePath + '/urldetails/' + urlDetailsId);
    expect(requestsDone.request.method).toEqual('GET');
    requestsDone.flush(dataToBeReturned);
  });


  it('getShortUrlDetailsFromId() should delete and retrieve short URL', () => {
    // [Given]
    const urlDetailsId = '1a2b3c4d5e';
    const dataToBeReturned = {
      shortUrlId: urlDetailsId, shortUrl: 'domain.ca/' + urlDetailsId,
      longUrl: 'http://long-url.ca/', creationTimestamp: ''
    };

    // [When]
    const urldetails: Observable<UrlDetails> = service.deleteShortUrlDetailsFromId(urlDetailsId);

    // [Then]
    urldetails.subscribe(data => expect(data).toEqual(dataToBeReturned));
    const requestsDone = httpTestingController.expectOne(basePath + '/urldetails/' + urlDetailsId);
    expect(requestsDone.request.method).toEqual('DELETE');
    requestsDone.flush(dataToBeReturned);
  });


  it('getLongUrlValue() should retrieve long URL', () => {
    // [Given]
    const shortUrlId = '1a2b3c4d5e';
    const shortUrl = 'domain.ca/' + shortUrlId;
    const dataToBeReturned = {
      shortUrlId, shortUrl,
      longUrl: 'http://long-url.ca/', creationTimestamp: ''
    };

    // [When]
    const urldetails: Observable<UrlDetails> = service.getLongUrlValue(shortUrl);

    // [Then]
    urldetails.subscribe(data => expect(data).toEqual(dataToBeReturned));
    const requestsDone = httpTestingController.expectOne(basePath + '/urlsearch?value=' + shortUrl.replace('/', '%2F'));
    expect(requestsDone.request.method).toEqual('GET');
    requestsDone.flush(dataToBeReturned);
  });


  it('getAllShortUrlDetails() should retrieve a page of URL details', () => {
    // [Given]
    const urlList = [{
      shortUrlId: '1a2b3c4d5e', shortUrl: 'domain.ca/1a2b3c4d5e',
      longUrl: 'http://long-url.ca/', creationTimestamp: ''
    }, {
      shortUrlId: '1a2b3c4d5f', shortUrl: 'domain.ca/1a2b3c4d5f',
      longUrl: 'http://long-url2.ca/', creationTimestamp: ''
    }];

    // [When]
    const urlPage: Observable<ShortUrlDetailsPage> = service.getAllShortUrlDetails(0, 5);

    // [Then]
    urlPage.subscribe(data => expect(data).toEqual(new ShortUrlDetailsPage(0, 5, 2, 1, urlList)));
    const requestsDone = httpTestingController.expectOne(basePath + '/urlsearch/list?page=0&size=5');
    expect(requestsDone.request.method).toEqual('GET');
    requestsDone.flush(urlList, {
      headers: new HttpHeaders()
        .set('x-page-number', '0')
        .set('x-page-size', '5')
        .set('x-total-results', '2')
        .set('x-total-pages', '1')
    });
  });


  it('getAllShortUrlDetails() should retrieve a page of URL details even without response headers', () => {
    // [Given]
    const urlList = [{
      shortUrlId: '1a2b3c4d5e', shortUrl: 'domain.ca/1a2b3c4d5e',
      longUrl: 'http://long-url.ca/', creationTimestamp: ''
    }, {
      shortUrlId: '1a2b3c4d5f', shortUrl: 'domain.ca/1a2b3c4d5f',
      longUrl: 'http://long-url2.ca/', creationTimestamp: ''
    }];

    // [When]
    const urlPage: Observable<ShortUrlDetailsPage> = service.getAllShortUrlDetails(0, 5);

    // [Then]
    urlPage.subscribe(data => expect(data).toEqual(new ShortUrlDetailsPage(0, 5, 2, 1, urlList)));
    const requestsDone = httpTestingController.expectOne(basePath + '/urlsearch/list?page=0&size=5');
    expect(requestsDone.request.method).toEqual('GET');
    requestsDone.flush(urlList);
  });

  
  it('getAllShortUrlDetails() should retrieve a page of URL details even without a body', () => {
    // [When]
    const urlPage: Observable<ShortUrlDetailsPage> = service.getAllShortUrlDetails(0, 5);

    // [Then]
    urlPage.subscribe(data => expect(data).toEqual(new ShortUrlDetailsPage(0, 5, 0, 1, [])));
    const requestsDone = httpTestingController.expectOne(basePath + '/urlsearch/list?page=0&size=5');
    expect(requestsDone.request.method).toEqual('GET');
    requestsDone.flush(null);
  });
});
