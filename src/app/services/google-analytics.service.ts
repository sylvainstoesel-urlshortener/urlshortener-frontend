import { Injectable } from '@angular/core';
import { Analytics, getAnalytics, logEvent } from 'firebase/analytics';
import { FirebaseApp, initializeApp } from 'firebase/app';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class GoogleAnalyticsService {
  private app?: FirebaseApp;
  private analytics?: Analytics;

  constructor() {
    const firebaseConfig = environment.firebaseConfig;
    if (firebaseConfig) {
      this.app = initializeApp(firebaseConfig);
      this.analytics = getAnalytics(this.app);
    }
  }

  logEvent(event: string): void {
    if (this.analytics) {
      logEvent(this.analytics, event, { production: environment.production });
    }
  }

  public logMainScreenLoad(): void {
    this.logEvent('MAIN_SCREEN_LOAD');
  }
  public logMainScreenShortenUrl(): void {
    this.logEvent('MAIN_SCREEN_SHORTEN_URL');
  }
  public logMainScreenRetrieveUrl(): void {
    this.logEvent('MAIN_SCREEN_RETRIEVE_URL');
  }

  public logRedirectionScreenLoad(): void {
    this.logEvent('REDIRECTION_SCREEN_LOAD');
  }
  public logRedirectionScreenRedirectToUrl(): void {
    this.logEvent('REDIRECTION_SCREEN_REDIRECT_TO_URL');
  }

  public logAdminScreenLoad(): void {
    this.logEvent('ADMIN_SCREEN_LOAD');
  }
  public logAdminScreenPageChange(): void {
    this.logEvent('ADMIN_SCREEN_PAGE_CHANGE');
  }
  public logAdminScreenDeleteUrl(): void {
    this.logEvent('ADMIN_SCREEN_DELETE_URL');
  }
}
