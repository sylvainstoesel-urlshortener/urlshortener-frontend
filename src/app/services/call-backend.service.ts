import { HttpResponse } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UrlDetailsService, UrlParametersService, UrlSearchService } from 'src/generated/services';
import { UrlDetails, UrlParameters } from '../../generated/models';
import { ShortUrlDetailsPage } from '../models/short-url-details-page.model';

@Injectable({
  providedIn: 'root',
})
export class CallBackendService {
  private urlParametersService = inject(UrlParametersService);
  private urlDetailsService = inject(UrlDetailsService);
  private urlSearchService = inject(UrlSearchService);

  public getApiUrlParameters(): Observable<UrlParameters> {
    return this.urlParametersService.getUrlShortenerParameters();
  }

  public createShortUrlFromLongUrl(
    longUrlValue: string
  ): Observable<UrlDetails> {
    return this.urlDetailsService.createShortenedUrl({ body: { value: longUrlValue } });
  }

  public getShortUrlDetailsFromId(shortUrlId: string): Observable<UrlDetails> {
    return this.urlDetailsService.getUrlDetails({ shortUrlId });
  }

  public deleteShortUrlDetailsFromId(shortUrlId: string): Observable<UrlDetails> {
    return this.urlDetailsService.deleteUrlDetails({ shortUrlId });
  }

  public getLongUrlValue(shortUrl: string): Observable<UrlDetails> {
    return this.urlSearchService.searchUrlDetails({ value: shortUrl });
  }

  public getAllShortUrlDetails(page: number, size: number): Observable<ShortUrlDetailsPage> {
    return this.urlSearchService
      .listAllShortenedUrls$Response({ page, size })
      .pipe(
        map((data: HttpResponse<UrlDetails[]>) => {
          const newPage = data.headers.get('x-page-number');
          const newSize = data.headers.get('x-page-size');
          const newCount = data.headers.get('x-total-results');
          const newPageCount = data.headers.get('x-total-pages');
          const body = data.body || [];
          return new ShortUrlDetailsPage(
            newPage ? parseInt(newPage, 10) : page,
            newSize ? parseInt(newSize, 10) : size,
            newCount ? parseInt(newCount, 10) : page * size + body.length,
            newPageCount ? parseInt(newPageCount, 10) : page + 1,
            body
          );
        })
      );
  }
}
