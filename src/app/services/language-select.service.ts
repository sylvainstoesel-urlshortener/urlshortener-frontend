import { inject, Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { getBrowserLang, TranslocoService } from '@jsverse/transloco';
import { AVAILABLE_LANGUAGES, AVAILABLE_LANGUAGES_CONST, DEFAULT_LANGUAGE } from '../transloco/transloco-root.module';

@Injectable({
  providedIn: 'root'
})
export class LanguageSelectService {

  private titleService = inject(Title);
  private translocoService = inject(TranslocoService);

  confirmLanguageChange(language: string): typeof AVAILABLE_LANGUAGES_CONST[number] {
    // Check that language exists.
    if (!AVAILABLE_LANGUAGES.includes(language)) {
      // If it doesn't exist, use the language of the browser or the default one. 
      const browserLang = getBrowserLang();
      language = browserLang && AVAILABLE_LANGUAGES.includes(browserLang) ? browserLang : DEFAULT_LANGUAGE;
    }
    // Update the language in Transloco.
    this.translocoService.setActiveLang(language);
    // Update the application title in the web page.
    this.translocoService
      .selectTranslate('app.title')
      .subscribe((value: string) => {
      if (value) {
        this.titleService.setTitle(value);
      }
    });
    return language as typeof AVAILABLE_LANGUAGES_CONST[number];
  }

  getTranslation(key: string): string {
    return this.translocoService.translate(key);
  }
}
