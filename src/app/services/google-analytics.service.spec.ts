import { TestBed } from '@angular/core/testing';

import { provideExperimentalZonelessChangeDetection } from '@angular/core';
import { GoogleAnalyticsService } from './google-analytics.service';

describe('GoogleAnalyticsService', () => {
  let service: GoogleAnalyticsService;
  let logEventSpy: any;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        provideExperimentalZonelessChangeDetection(),
      ]
    });
    service = TestBed.inject(GoogleAnalyticsService);

    logEventSpy = spyOn(service, 'logEvent');
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('logMainScreenLoad should log event', () => {
    // [When]
    service.logMainScreenLoad();
    // [Then]
    expect(logEventSpy).toHaveBeenCalledWith('MAIN_SCREEN_LOAD');
  })
  it('logMainScreenShortenUrl should log event', () => {
    // [When]
    service.logMainScreenShortenUrl();
    // [Then]
    expect(logEventSpy).toHaveBeenCalledWith('MAIN_SCREEN_SHORTEN_URL');
  })
  it('logMainScreenRetrieveUrl should log event', () => {
    // [When]
    service.logMainScreenRetrieveUrl();
    // [Then]
    expect(logEventSpy).toHaveBeenCalledWith('MAIN_SCREEN_RETRIEVE_URL');
  })

  it('logRedirectionScreenLoad should log event', () => {
    // [When]
    service.logRedirectionScreenLoad();
    // [Then]
    expect(logEventSpy).toHaveBeenCalledWith('REDIRECTION_SCREEN_LOAD');
  })
  it('logRedirectionScreenRedirectToUrl should log event', () => {
    // [When]
    service.logRedirectionScreenRedirectToUrl();
    // [Then]
    expect(logEventSpy).toHaveBeenCalledWith('REDIRECTION_SCREEN_REDIRECT_TO_URL');
  })

  it('logAdminScreenLoad should log event', () => {
    // [When]
    service.logAdminScreenLoad();
    // [Then]
    expect(logEventSpy).toHaveBeenCalledWith('ADMIN_SCREEN_LOAD');
  })
  it('logAdminScreenPageChange should log event', () => {
    // [When]
    service.logAdminScreenPageChange();
    // [Then]
    expect(logEventSpy).toHaveBeenCalledWith('ADMIN_SCREEN_PAGE_CHANGE');
  })
  it('logAdminScreenDeleteUrl should log event', () => {
    // [When]
    service.logAdminScreenDeleteUrl();
    // [Then]
    expect(logEventSpy).toHaveBeenCalledWith('ADMIN_SCREEN_DELETE_URL');
  })
});
