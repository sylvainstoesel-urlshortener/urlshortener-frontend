import { provideExperimentalZonelessChangeDetection } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { Title } from '@angular/platform-browser';
import { TranslocoService } from '@jsverse/transloco';
import { TranslocoTestRootModule } from '../transloco/transloco-testing.module.spec';
import { LanguageSelectService } from './language-select.service';


describe('LanguageSelectService', () => {
  let service: LanguageSelectService;
  
  let spyTitle: jasmine.Spy;
  let spyTransloco : jasmine.Spy;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TranslocoTestRootModule],
      providers: [
        Title,
        LanguageSelectService,
        provideExperimentalZonelessChangeDetection(),
      ]
    });
    service = TestBed.inject(LanguageSelectService);
    spyTransloco = spyOn(TestBed.inject(TranslocoService), 'setActiveLang').and.callThrough();
    spyTitle = spyOn(TestBed.inject(Title), 'setTitle').and.callThrough();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('confirmLanguageChange()', () => {
    it('should return "en" for "en"', () => {
      // [When]
      const result = service.confirmLanguageChange('en');
      // [Then]
      expect(result).toEqual('en');
      expect(spyTransloco.calls.first().args[0]).toEqual('en');
      expect(spyTitle.calls.first().args[0]).toEqual('Sylvain Stoesel’s Url Shortener');
    });

    it('should change language to "fr" and return "fr" for "fr"', () => {
      // [When]
      const result = service.confirmLanguageChange('fr');
      // [Then]
      expect(result).toEqual('fr');
      expect(spyTransloco.calls.first().args[0]).toEqual('fr');
      expect(spyTitle.calls.first().args[0]).toEqual('Raccourcisseur d’URL de Sylvain Stoesel');
    });

    it('should change language to browser language or default for "pl" (language not supported by the app)', () => {
      // [When]
      const result = service.confirmLanguageChange('pl');
      // [Then]
      expect(['en', 'fr']).toContain(result);
      expect(['en', 'fr']).toContain(spyTransloco.calls.first().args[0]);
      expect(['Sylvain Stoesel’s Url Shortener', 'Raccourcisseur d’URL de Sylvain Stoesel'])
        .toContain(spyTitle.calls.first().args[0]);
    });
  });

  describe('getTranslation()', () => {
    it('should return the translation in the current language for a given key', () => {
      // [When]
      const translation = service.getTranslation('app.title');
      // [Then]
      expect(translation).toEqual('Sylvain Stoesel’s Url Shortener');
    });
  });
});
