import { computed, inject } from '@angular/core';
import { tapResponse } from '@ngrx/operators';
import { patchState, signalStore, withComputed, withMethods, withState } from '@ngrx/signals';
import { rxMethod } from '@ngrx/signals/rxjs-interop';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { filter, pipe, switchMap, tap } from 'rxjs';
import { ApiError, UrlParameters } from 'src/generated/models';
import { CallBackendService } from '../services/call-backend.service';
import { LanguageSelectService } from '../services/language-select.service';
import { AVAILABLE_LANGUAGES_CONST, DEFAULT_LANGUAGE } from '../transloco/transloco-root.module';

/**
 * The UrlShortener Store.
 * ----------------------
 * This store the initial state and its type (INITIAL_URLSHORTENER_STATE), containing:
 *  - the URL parameters obtained from the back-end endpoint GET /urlparameters.
 *  - whether the app is currently loading information from that endpoint (and the error, if any)
 *  - the chosen language.
 * The store also declares:
 *  - extra-properties computed from the UrlShortenerState (apiVersion, apiErrorMessage)
 *  - the methods that can be called to modify the state (actions)
 *  - the side-effects (methods executed asynchronously when some actions are triggered).
 */

const INITIAL_URLSHORTENER_STATE = {
  urlParameters: {
    apiVersion: 'x.x.x',
    shortUrlDomain: '/',
    shortUrlIdSize: 10,
  } as UrlParameters,
  isLoading: false,
  apiError: undefined as ApiError | undefined,
  language: DEFAULT_LANGUAGE as typeof AVAILABLE_LANGUAGES_CONST[number],
};


export const UrlShortenerStore = signalStore({ providedIn: 'root' },

  withState(INITIAL_URLSHORTENER_STATE), // Definition of initial state.

  withComputed(({ urlParameters, apiError }) => ({
    // Extra properties - Computed parts of the store (part of UrlShortenerState)
    apiVersion: computed(() => urlParameters().apiVersion),
    apiErrorMessage: computed(() => apiError()?.message || ''),
  })),

  withMethods((
    store,
    callBackendService = inject(CallBackendService),
    languageSelectService = inject(LanguageSelectService),
    storageService = inject(LOCAL_STORAGE) as StorageService
  ) => {
    const LANGUAGE_SETTINGS_STORAGE_KEY = 'UrlShortenerLanguageSelected';

    // Change language in store, after verifying that it is a valid one, and update browser storage as well.
    function changeLanguage(language: string): void {
      const acceptedLanguage = languageSelectService.confirmLanguageChange(language);
      if (language !== acceptedLanguage) {
        console.log(`[UrlShortenerStore] Language asked: ${language} | Language selected: ${acceptedLanguage}`);
      } else {
        console.log(`[UrlShortenerStore] Language selected: ${language}`);
      }
      storageService.set(LANGUAGE_SETTINGS_STORAGE_KEY, acceptedLanguage);
      patchState(store, { language: acceptedLanguage });
    };

    // Call back-end to get URL parameters, and manage loading/error if any.
    const loadApiUrlParameters = rxMethod<void>(
      pipe(
        filter(() => !store.isLoading()),
        tap(() => patchState(store, { isLoading: true })),
        switchMap(() => {
          return callBackendService.getApiUrlParameters().pipe(
            tapResponse({
              next: (urlParameters) => patchState(store, { urlParameters, apiError: undefined }),
              error: (apiError: ApiError) => patchState(store, { apiError }),
              finalize: () => patchState(store, { isLoading: false }),
            })
          );
        })
      ),
    );

    // Load everything when starting the app: get saved language in browser storage, and call both previous methods.
    function loadMainPage(): void {
      const language = storageService.get(LANGUAGE_SETTINGS_STORAGE_KEY) as string;
      changeLanguage(language);
      loadApiUrlParameters();
    };

    return { loadMainPage, changeLanguage }; // We just want to publicly expose these 2 methods
  })
);
