import { provideExperimentalZonelessChangeDetection } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { delay, of, throwError } from 'rxjs';
import { ApiError } from 'src/generated/models';
import { CallBackendService } from '../services/call-backend.service';
import { LanguageSelectService } from '../services/language-select.service';
import { generateApiUrlParameters } from '../utils/testing-mock-service.spec';
import { UrlShortenerStore } from './url-shortener.store';

describe('UrlShortenerStore', () => {
  let callBackendService: jasmine.SpyObj<CallBackendService>;
  let languageSelectService: jasmine.SpyObj<LanguageSelectService>;
  let storageService: jasmine.SpyObj<StorageService>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        UrlShortenerStore,
        {
          provide: LanguageSelectService,
          useValue: jasmine.createSpyObj('LanguageSelectService', ['confirmLanguageChange']),
        },
        {
          provide: CallBackendService,
          useValue: jasmine.createSpyObj('CallBackendService', ['getApiUrlParameters']),
        },
        {
          provide: LOCAL_STORAGE,
          useValue: jasmine.createSpyObj('StorageService', ['get', 'set']),
        },
        provideExperimentalZonelessChangeDetection(),
      ]
    });

    languageSelectService = TestBed.inject(LanguageSelectService) as jasmine.SpyObj<LanguageSelectService>;
    callBackendService = TestBed.inject(CallBackendService) as jasmine.SpyObj<CallBackendService>;
    storageService = TestBed.inject(LOCAL_STORAGE) as jasmine.SpyObj<StorageService>;
  });

  describe('withComputed', () => {
    it(`apiVersion() should return apiVersion`, () => {
      // [Given]
      const store = TestBed.inject(UrlShortenerStore);
      // [When]
      const result = store.apiVersion();
      // [Then]
      expect(result).toEqual('x.x.x');
    });

    it(`apiErrorMessage() should return apiErrorMessage`, () => {
      // [Given]
      const store = TestBed.inject(UrlShortenerStore);
      // [When]
      const result = store.apiErrorMessage();
      // [Then]
      expect(result).toEqual('');
    });
  });

  describe('withMethods', () => {
    describe('changeLanguage()', () => {
      it(`should change language if it is a valid language`, () => {
        // [Given]
        const store = TestBed.inject(UrlShortenerStore);
        languageSelectService.confirmLanguageChange.and.returnValue('es');
        // [When]
        store.changeLanguage('es');
        // [Then]
        expect(store.language()).toEqual('es');
        expect(languageSelectService.confirmLanguageChange.calls.count()).toEqual(1);
        expect(languageSelectService.confirmLanguageChange.calls.first().args).toEqual(['es']);
        expect(storageService.set.calls.count()).toEqual(1);
        expect(storageService.set.calls.first().args).toEqual(['UrlShortenerLanguageSelected', 'es']);
      });

      it(`should not change language if it isn't a valid language`, () => {
        // [Given]
        const store = TestBed.inject(UrlShortenerStore);
        languageSelectService.confirmLanguageChange.and.returnValue('fr');
        // [When]
        store.changeLanguage('pl');
        // [Then]
        expect(store.language()).toEqual('fr');
        expect(languageSelectService.confirmLanguageChange.calls.count()).toEqual(1);
        expect(languageSelectService.confirmLanguageChange.calls.first().args).toEqual(['pl']);
        expect(storageService.set.calls.count()).toEqual(1);
        expect(storageService.set.calls.first().args).toEqual(['UrlShortenerLanguageSelected', 'fr']);
      });
    });

    describe('loadMainPage()', () => {
      it(`should load main page with valid backend response`, () => {
        // [Given]
        const urlParameters = generateApiUrlParameters();
        const store = TestBed.inject(UrlShortenerStore);
        storageService.get.and.returnValue('it');
        languageSelectService.confirmLanguageChange.and.returnValue('it');
        callBackendService.getApiUrlParameters.and.returnValue(of(urlParameters));
        // [When]
        store.loadMainPage();
        // [Then]
        expect(store.isLoading()).toEqual(false);
        expect(store.urlParameters()).toEqual(urlParameters);
        expect(store.apiError()).toBeUndefined();
        expect(store.language()).toEqual('it');
        expect(storageService.get.calls.count()).toEqual(1);
        expect(storageService.get.calls.first().args).toEqual(['UrlShortenerLanguageSelected']);
        expect(languageSelectService.confirmLanguageChange.calls.count()).toEqual(1);
        expect(languageSelectService.confirmLanguageChange.calls.first().args).toEqual(['it']);
        expect(storageService.set.calls.count()).toEqual(1);
        expect(storageService.set.calls.first().args).toEqual(['UrlShortenerLanguageSelected', 'it']);
        expect(callBackendService.getApiUrlParameters.calls.count()).toEqual(1);
      });

      it(`should load main page with invalid backend response`, () => {
        // [Given]
        const apiError = { code: 'ERR' } as ApiError;
        const store = TestBed.inject(UrlShortenerStore);
        storageService.get.and.returnValue('it');
        languageSelectService.confirmLanguageChange.and.returnValue('it');
        callBackendService.getApiUrlParameters.and.returnValue(throwError(() => apiError));
        // [When]
        store.loadMainPage();
        // [Then]
        expect(store.isLoading()).toEqual(false);
        expect(store.apiError()).toEqual(apiError);
        expect(store.language()).toEqual('it');
        expect(storageService.get.calls.count()).toEqual(1);
        expect(storageService.get.calls.first().args).toEqual(['UrlShortenerLanguageSelected']);
        expect(languageSelectService.confirmLanguageChange.calls.count()).toEqual(1);
        expect(languageSelectService.confirmLanguageChange.calls.first().args).toEqual(['it']);
        expect(storageService.set.calls.count()).toEqual(1);
        expect(storageService.set.calls.first().args).toEqual(['UrlShortenerLanguageSelected', 'it']);
        expect(callBackendService.getApiUrlParameters.calls.count()).toEqual(1);
      });

      it(`should load main page with slow backend response and not call again backend if still loading`, (done) => {
        const urlParameters = generateApiUrlParameters();
        const store = TestBed.inject(UrlShortenerStore);
        storageService.get.and.returnValue('it');
        languageSelectService.confirmLanguageChange.and.returnValue('it');
        callBackendService.getApiUrlParameters.and.returnValue(of(urlParameters).pipe(delay(500)));
        // [When]
        store.loadMainPage();
        // [Then]
        expect(store.isLoading()).toEqual(true);
        expect(store.urlParameters().apiVersion).toEqual('x.x.x'); // still initial state
        store.loadMainPage(); // This 2 calls should not trigger calling the backend.
        store.loadMainPage();
        expect(callBackendService.getApiUrlParameters.calls.count()).toEqual(1);
        expect(storageService.get.calls.count()).toEqual(3);
        new Promise((resolve) => setTimeout(resolve, 600)).then(() => {
          expect(store.isLoading()).toEqual(false);
          expect(store.urlParameters()).toEqual(urlParameters);
          store.loadMainPage(); // Reload the main page now that the backend has responded.
          expect(store.isLoading()).toEqual(true);
          expect(callBackendService.getApiUrlParameters.calls.count()).toBe(2);
          expect(storageService.get.calls.count()).toEqual(4);
          new Promise((resolve) => setTimeout(resolve, 600)).then(() => {
            expect(store.isLoading()).toEqual(false);
            expect(store.urlParameters()).toEqual(urlParameters);
            done();
          });
        });
      });
    });
  });
});
