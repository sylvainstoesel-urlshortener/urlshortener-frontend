import { Routes, UrlSegment } from '@angular/router';
import { AccordionComponent } from './components/accordion/accordion.component';
import { ListAllUrlsComponent } from './components/list-all-urls/list-all-urls.component';
import { RedirectLongUrlComponent } from './components/redirect-long-url/redirect-long-url.component';

export function isShortUrlId(url: UrlSegment[]): any {
  return url.length === 1 && url[0].path.match(/^[A-Za-z\d]{5,30}$/g) ? ({ consumed: url }) : null;
}

export const APP_ROUTES: Routes = [
  { path: '', component: AccordionComponent },
  { path: 'admin', component: ListAllUrlsComponent },
  { matcher: isShortUrlId, component: RedirectLongUrlComponent },
  { path: '**', redirectTo: '', pathMatch: 'full' }
];
