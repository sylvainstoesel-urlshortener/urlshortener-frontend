@ECHO OFF

set wiremockVersion=3.2.0

if not exist "%cd%\wiremock-standalone-%wiremockVersion%.jar" (
   bitsadmin.exe /transfer "download" https://repo1.maven.org/maven2/org/wiremock/wiremock-standalone/%wiremockVersion%/wiremock-standalone-%wiremockVersion%.jar "%cd%\wiremock-standalone-%wiremockVersion%.jar"
)

@ECHO ON
java -jar wiremock-standalone-%wiremockVersion%.jar --port 8080 --verbose --global-response-templating