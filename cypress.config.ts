import { addCucumberPreprocessorPlugin } from '@badeball/cypress-cucumber-preprocessor';
import webpack from '@cypress/webpack-preprocessor';
import { defineConfig } from 'cypress';

async function setupNodeEvents(
  on: Cypress.PluginEvents,
  config: Cypress.PluginConfigOptions
): Promise<Cypress.PluginConfigOptions> {
  // This is required for the preprocessor to be able to generate JSON reports after each run, and more,
  await addCucumberPreprocessorPlugin(on, config);

  on('before:browser:launch', (browser, launchOptions) => {
    if (browser.name === 'chrome') {
        launchOptions.args.push('--lang=en-US');
        launchOptions.args.push('--window-size=1420,960');
    }
    else if (browser.family === 'firefox') {
        launchOptions.preferences['lang'] = 'en-US';
    }
    else if (browser.name === 'electron') {
        launchOptions.preferences.lang = 'en-US';
    }
    return launchOptions;
});


  on(
    "file:preprocessor",
    webpack({
      webpackOptions: {
        resolve: {
          extensions: [".ts", ".js"],
        },
        module: {
          rules: [
            {
              test: /\.ts$/,
              exclude: [/node_modules/],
              use: [
                {
                  loader: "ts-loader",
                },
              ],
            },
            {
              test: /\.feature$/,
              use: [
                {
                  loader: "@badeball/cypress-cucumber-preprocessor/webpack",
                  options: config,
                },
              ],
            },
          ],
        },
      },
    })
  );

  // Make sure to return the config object as it might have been modified by the plugin.
  return config;
}

export default defineConfig({
  e2e: {
    specPattern: "e2e/features/*.feature",
    baseUrl: 'http://localhost:4200',
    supportFile: "e2e/support/index.ts",
    viewportHeight: 820,
    viewportWidth: 1000,
    trashAssetsBeforeRuns: true,
    downloadsFolder: "e2e-reports/downloads",
    screenshotsFolder: "e2e-reports/screenshots",
    videosFolder: "e2e-reports/videos",
    videoCompression: 15,
    video: true, // Put to false to accelerate execution of integration tests in CI
    chromeWebSecurity: false,
    setupNodeEvents,
  },
});