![Creative Commons BY-NC-SA license](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png) 
This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/).  
 &nbsp;  
# Sylvain Stoesel's Url Shortener - Angular Front-end  

*This is a demo-project aimed at showing how to use a bunch of technologies and frameworks (Angular, Bootstrap, etc.) to
implement a simple feature.  
This is the repository of the front-end part of the projet. The repository of the back-end part
is [here](https://gitlab.com/sylvainstoesel-urlshortener/urlshortener-backend).*  
  

## Make any link or URL shorter.

**This app is able to shorten and store any web URL. Shortened URLs can be viewed, accessed and deleted.  
If you click on a shortened URL, the app will redirect you to the original URL you provided.**


## Application links

| ENVIRONMENT   |                     Front-End                     |                        Back-End / API  (Swagger-UI link)                              |
|---------------|:-------------------------------------------------:|:-------------------------------------------------------------------------------------:|
| PRODUCTION    | [**sshrturl.web.app**](https://sshrturl.web.app/) | [**sshrturl.appspot.com**](https://sshrturl.appspot.com/swagger-ui.html)               |
| *LOCAL*       | [*localhost:4200*](http://localhost:4200/)        | [*localhost:8080*](http://localhost:8080/swagger-ui.html)                             |


| Other links                                  |                                                                                                                                                                                                  	|
|--------------------------------------------- |--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------	|
| Gitlab CI pipeline                           | [sylvainstoesel-urlshortener/urlshortener-frontend/-/pipelines](https://gitlab.com/sylvainstoesel-urlshortener/urlshortener-frontend/-/pipelines?ref=master)                       |
| Unit tests Karma report                      | [sylvainstoesel-urlshortener.gitlab.io/urlshortener-frontend/karma](https://sylvainstoesel-urlshortener.gitlab.io/urlshortener-frontend/karma/index.html)                             |
| Cypress/Cucumber automated tests report      | [sylvainstoesel-urlshortener.gitlab.io/urlshortener-frontend/cypress/cucumber-report.html](https://sylvainstoesel-urlshortener.gitlab.io/urlshortener-frontend/cypress/cucumber-report.html)       |
| Cypress/Cucumber recorded videos report      | [sylvainstoesel-urlshortener.gitlab.io/urlshortener-frontend/cypress/videos](https://sylvainstoesel-urlshortener.gitlab.io/urlshortener-frontend/cypress/videos/index.html)                    |
| SonarQube analysis (SonarCloud)              | [sonarcloud.io/dashboard?id=sylvainstoesel-urlshortener_urlshortener-frontend](https://sonarcloud.io/dashboard?id=sylvainstoesel-urlshortener_urlshortener-frontend)               |
| ~~Stryker Mutation Testing report~~          | [~~sylvainstoesel-urlshortener.gitlab.io/urlshortener-frontend/stryker~~](https://sylvainstoesel-urlshortener.gitlab.io/urlshortener-frontend/stryker/index.html)                           |

## Front-End Technologies

![Technologies](documentation/UrlShortenerFrontendTechnologies.svg)

**Version 19** of **Angular** is currently used in this project. To upgrade dependencies, run the following command: 

`ng update @angular/cdk@19 @angular/cli@19 @angular/core@19 @angular/material@19 @angular-eslint/schematics@19 --allow-dirty --force`
  
  

## Local build, test & deploy

### Cloning Project

```bash
git clone --recurse-submodules https://gitlab.com/sylvainstoesel-urlshortener/urlshortener-frontend.git

# To update the OpenAPI Contract submodule:
git submodule update --remote

# To update this code and the submodule:
git pull --recurse-submodules origin master

```



### Local server

*Prerequisites:* [Node 20 + npm 9](https://nodejs.org/en/download/) locally installed and [Angular CLI](https://angular.io/guide/setup-local#install-the-angular-cli)

Run `ng serve` for a local dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files. 

> This requires the **UrlShortener API running on port 8080**. 

You can either:
 - [locally deploy the backend](https://gitlab.com/sylvainstoesel-urlshortener/urlshortener-backend#locally-deploy-project)
or
 - start the [WireMock](http://wiremock.org/) standalone server. Stubs and mocks are provided in the [**wiremock**](wiremock/) folder. You will need to download the latest [standalone jar](https://repo1.maven.org/maven2/com/github/tomakehurst/wiremock-standalone/2.27.2/wiremock-standalone-2.27.2.jar) and move it into the **wiremock** folder.
    - On Windows, just execute the [start-wiremock.bat](wiremock/start-wiremock.bat) file. It will automatically download WireMock standalone jar and start the WireMock server on port 8080.
    - On other systems, run `java -jar wiremock-standalone-2.27.2.jar --port 8080 --verbose --global-response-templating` to start the server.
    - This also requires a [Java JRE or JDK](https://openjdk.java.net/projects/jdk/17/) locally installed.

### Build

**Run `npm run build` to build the project** (this includes optimizations).

There is a pre-build step that will automatically run when you run this command : `openapi-generate`.

This step generates into the [src/generated](src/generated) folder the **models** and **api clients** (Angular services) corresponding to the [**URL Shortener API contract**](https://gitlab.com/sylvainstoesel-urlshortener/urlshortener-backend-contract/-/blob/master/openapi-contract.yaml) ([used in the Git Submodule here](src/openapi)).
This is the Swagger/OpenAPI definition of the [Java Backend](https://gitlab.com/sylvainstoesel-urlshortener/urlshortener-backend).


### Running unit tests

Run `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).

Unit tests are executed on a headless Chrome.

### Running integration tests

Run `npm run e2e` to execute the *end-to-end* **integration tests** via [Cypress](https://www.cypress.io/).

Technically, these are *end-to-end* tests of the front-end part only. The back-end part will be mocked thanks to **Cypress**. 

These tests use **Behavior Driven Development** method thanks to the **Cucumber** framework. Integration tests
describes the behavior of the frontend and each of its features, and development is made afterwards, accordingly to the
described behaviors.

The tests are written in Gherkin language and assembled as [features](e2e/features) for Cucumber tests.
Gherkin language is basically English, which means anyone (and not only developers) can understand it.
Every "sentence" is a step that is linked to a piece of [TypeScript code](e2e/steps) (click on a button, assertions, etc.). 

An HTML report (along with a JSON report) is generated at the end of the tests, including snapshots of the tested pages. It's available [here](https://sylvainstoesel-urlshortener.gitlab.io/urlshortener-frontend/cypress/cucumber-report.html).
Moreover, video are recorded for each executed feature. You can see the application inside the browser being controlled by the automated tests.
You can access the videos from [this page](https://sylvainstoesel-urlshortener.gitlab.io/urlshortener-frontend/cypress/videos/index.html).  


### Mutation Testing
To run the [Stryker Mutation Testing](https://stryker-mutator.io/) on existing unit tests, run `npm run mutation-testing`.
This will compute the [mutation score](https://stryker-mutator.io/docs/mutation-testing-elements/mutant-states-and-metrics/) (strength of the unit tests) and generate an HTML report (locally in /coverage/stryker).


### All-in-one command

Just run `npm run all` to execute all the steps in the following order:

 - clean the generated folders: *dist*, *coverage*, *e2e-reports* (`npm run clean`)
 - install all the project dependencies (`npm install`)
 - build the project (`npm run build`). This includes the *OpenApi Generator* pre-build step (`npm run openapi-generate`)
 - check for code style (`npm run lint`)
 - execute the Karma unit tests (`npm run test`)
 - execute the Cypress integration tests (`npm run e2e`)

Because it is very slow, **mutation testing** is **not** included in the `npm run all` command.



## Deploy on production

#### Install firebase tools
```npm install -g firebase-tools```

#### Deploy

This requires a Google Account on which access has been granted to **sshrturl** project (owned by me).

```firebase deploy --only hosting --project sshrturl```

## Other tools & libraries used in this project

### NgRx Store - Managing global state of the app

**[NgRx](https://ngrx.io/guide/signals/signal-store/)** is a tool to manage the global state of the front-end application, using Angular Signals.

"Global state" is an object containing pieces of information shared between all the components of the app.
Here, it contains the language setting, and the `urlParameters` object coming from the back-end (API version, size of ID, ...).
Instead of having to cache these information in an Angular `Service`, we store it in the **[UrlShortener Signal-Store](src/app/store/url-shortener.store.ts)**.
We can then move complex logic from the components to the store, and keep an up-to-date state of the application that each component can easily access. The state can be accessed from everywhere in the app as an Angular `Signal`.

### BDD (Behavior Driven Development) & automated integration tests in Cypress

This project uses **Behavior Driven Development** method thanks to the **Cucumber** framework.
Integration tests describes the behavior of the API and each of its endpoints, and development is made afterwards, accordingly to the described behaviors.

The tests are written in Gherkin language and assembled as [features](e2e/features/) for Cucumber tests.
Gherkin language is basically English, which means anyone (and not only developers) can understand it.
Every "sentence" is a step that is linked to a piece of Typescript/Cypress code (loading screen, clicking on buttons, assertions on visible elements, etc.).

During the integration test phase, API responses (used by the front-end) are mocked using **Cypress** framework.

### Continuous Integration & Deployment

[**Gitlab CI**](https://docs.gitlab.com/ee/ci/) is used for building, testing and deploying to production.  
The pipeline is triggered on a commit on *master* branch. The pipeline will:
  - Build the project:
    - install all needed npm dependencies (`npm install`)
    - build the application for production (`npm run build`)
    - check coding style (`npm run lint`)
  - Test the project:
    - run the unit tests (`npm run test`)
    - run the integration/e2e tests (`npm run e2e`)
    - ~~run the mutation testing on unit tests (`npm run mutation-testing`)~~
  - publish the Karma Unit Tests and Cypress/Cucumber HTML+videos reports on [**GitLab Pages**](https://docs.gitlab.com/ee/user/project/pages/)
  - run the [SonarQube](https://www.sonarqube.org/) analysis and publish the report on [SonarCloud.io](https://sonarcloud.io/dashboard?id=sylvainstoesel-urlshortener_urlshortener-frontend)
  - deploy the application on [Google Firebase](https://console.firebase.google.com/u/0/project/sshrturl/overview) hosting service (production server)

Also, the pipeline is triggered when creating a **Merge Request** (or **Pull Request**) targetting the *master* branch.
The same steps will be executed, except publishing the reports on **GitLab Pages** and deploying the application.