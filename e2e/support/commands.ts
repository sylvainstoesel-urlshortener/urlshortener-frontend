import { attach } from '@badeball/cypress-cucumber-preprocessor';


declare global {
  namespace Cypress {
    interface Chainable<Subject> {
      screenshotAll(): Chainable<null>;

      checkValueExpected(expected: string, obtained: any, description?: string): Cypress.Chainable<void>;
      checkElementTextExpected(expected: string, element: Cypress.Chainable<JQuery<HTMLElement>>, description?: string): Cypress.Chainable<void>;

      getBySel(selector: string, args?: any): Cypress.Chainable<JQuery<HTMLElement>>;
      findBySel(selector: string, args?: any): Cypress.Chainable<JQuery<HTMLElement>>;

      getByDataTestClass(selector: string, args?: any): Cypress.Chainable<JQuery<HTMLElement>>;
      findByDataTestClass(selector: string, args?: any): Cypress.Chainable<JQuery<HTMLElement>>;

      findAccordionPanelSelectorForName(name: string): Cypress.Chainable<string>;
    }
  }
}

function checkValueExpected(expected: string, obtained: any, description?: string) {
  if (expected.trim() !== ('' + obtained).trim()) {
    attach((description ? description : 'Value') + ` unexpected: "${obtained}" (expected was "${expected}")`);
  }
  else {
    attach((description ? description : 'Value') + ` obtained: "${obtained}"`);
  }
  expect(('' + obtained).trim()).to.equal(expected.trim());
}

Cypress.Commands.addAll({

  screenshotAll(): Cypress.Chainable<undefined> {
    return cy.screenshot({capture: 'runner'});
  },

  checkValueExpected(expected: string, obtained: string, description?: string): void {
    checkValueExpected(expected, obtained, description);
  },

  checkElementTextExpected(expected: string, element: Cypress.Chainable<JQuery<HTMLElement>>, description?: string): void {
    element.invoke('text').then((text) => {
      checkValueExpected(expected, text, description);
    });
  },

  getBySel(selector: string, ...args): Cypress.Chainable<JQuery<HTMLElement>> {
    return cy.get(`[data-test='${selector}']`, ...args);
  },

  getByDataTestClass(selector: string, ...args): Cypress.Chainable<JQuery<HTMLElement>> {
    return cy.get(`.data-test-${selector}`, ...args);
  },

  findAccordionPanelSelectorForName(name: string) {
    let selector = '';
    cy.getBySel('accordion').find('mat-expansion-panel').each((panel, index) => {
      if (panel.find(`[data-test='accordion-panel-title']`).text().trim() === name) {
        selector = `accordion-panel-${index + 1}`;
      }
    }).then(() => {
      cy.wrap(selector);
    });
  }
});

Cypress.Commands.add('findBySel',
  { prevSubject: true },
  (subject: Cypress.Chainable<JQuery<HTMLElement>>, selector: string, ...args) => {
    return subject.find(`[data-test='${selector}']`, ...args);
  }
)
Cypress.Commands.add('findByDataTestClass',
  { prevSubject: true },
  (subject: Cypress.Chainable<JQuery<HTMLElement>>, selector: string, ...args) => {
    return subject.find(`.data-test-${selector}`, ...args);
  }
)