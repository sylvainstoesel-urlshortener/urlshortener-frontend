import { Then } from '@badeball/cypress-cucumber-preprocessor';


Then('after a while, I should see the closed accordion with the 2 features {string} and {string}',
(feat1: string, feat2: string) => {
  // Wait for spinner to disappear and check that it is gone
  cy.getBySel('loading-spinner', {timeout: 10000}).should('not.exist');
  // Check that both features are closed - only accordion titles are shown
  cy.getBySel('accordion-panel-1').should('not.have.class','mat-expanded');
  cy.getBySel('accordion-panel-2').should('not.have.class','mat-expanded');
  // Check features titles
  cy.checkElementTextExpected(feat1, cy.getBySel('accordion-panel-1').findBySel('accordion-panel-title'), 'Feature title 1');
  cy.checkElementTextExpected(feat2, cy.getBySel('accordion-panel-2').findBySel('accordion-panel-title'), 'Feature title 2');
  cy.screenshotAll();
});


Then('if I click on feature {string}, it opens the feature whose label is {string}', (name: string, label: string) => {
  // Get feature selector from feature name
  cy.findAccordionPanelSelectorForName(name).then((selector) => {
    // Check that feature is closed and not visible before clicking on it.
    cy.getBySel(selector).should('not.have.class','mat-expanded');
    cy.getBySel(selector).findBySel('form-legend').should('not.be.visible');
    // Click on feature title and wait a bit
    cy.getBySel(selector).findBySel('accordion-panel-title').click();
    cy.wait(800);
    // Check that feature is open and visible.
    cy.getBySel(selector).should('have.class','mat-expanded');
    cy.getBySel(selector).findBySel('form-legend').should('be.visible');
    // Check that label is the right one.
    cy.checkElementTextExpected(label, cy.getBySel(selector).findBySel('form-legend'), 'Feature label 1');
    cy.screenshotAll();
  });
});