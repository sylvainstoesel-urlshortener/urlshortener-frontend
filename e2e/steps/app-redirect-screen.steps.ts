import { Then, When, attach } from '@badeball/cypress-cucumber-preprocessor';

When('I navigate to the redirection page with short URL ID {string}', (shortUrlId: string) => {
  const URI = '/' + shortUrlId;
  attach(`Navigating to ${Cypress.config('baseUrl') + URI}`);
  cy.visit(URI, {
    onBeforeLoad(win) {
      cy.spy(win.console, 'error').as('consoleError');
    },
  });
});

Then('after a while, I should see a redirection message saying {string}', (message: string) => {
    // Wait for spinner to disappear and check that it is gone
    cy.getBySel('loading-spinner', {timeout: 10000}).should('not.exist');
    // Check message
    cy.checkElementTextExpected(message, cy.getBySel('redirect-message-1'), 'Redirection message');
    cy.screenshotAll();
});

Then('after a while, I should see a redirection message starting with {string}', (message: string) => {
    // Wait for spinner to disappear and check that it is gone
    cy.getBySel('loading-spinner', {timeout: 10000}).should('not.exist');
    // Check message
    cy.getBySel('redirect-message-2')
      .invoke('text')
      .then((text) => {
        attach(`Redirection message value: ${text}`);
        expect(text).to.include(message);
    })
    cy.screenshotAll();
});

Then('after a while, I should see an error {string} instead of the redirection message', (error: string) => {
    // Wait for spinner to disappear and check that it is gone
    cy.getBySel('loading-spinner', {timeout: 10000}).should('not.exist');
    // Check error message
    cy.checkElementTextExpected(error, cy.getBySel('redirect-error-message'), 'Redirection message');
    cy.screenshotAll();
});

Then('after {int} seconds, I should be redirected to URL {string}', (seconds: number, url: string) => {
    // Wait
    cy.wait(seconds * 1000);
    // Check browser URL
    cy.url().should('eq', url);
    cy.screenshotAll();
});
