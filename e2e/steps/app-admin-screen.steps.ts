import { Then, When, attach } from '@badeball/cypress-cucumber-preprocessor';

When('I navigate to the admin page', () => {
    const URI = '/admin';
  attach(`Navigating to ${Cypress.config('baseUrl') + URI}`);
  cy.visit(URI, {
    onBeforeLoad(win) {
      cy.spy(win.console, 'error').as('consoleError');
    },
  });
});
  
When('I click on the {string} page button', (label: string) => {
    cy.getBySel('pagination-table-url-list').find(`button.mat-mdc-paginator-navigation-${label.toLowerCase()}`).click();
});
  
When('I click on the trash button of the visible record #{int}', (rowIndex: number) => {
    cy.getBySel('trash-button-table-url-list').eq(rowIndex - 1).click();
});

When('I click on the {string} button inside the popup', (label: string) => {
    // Check that popup is visible
    cy.get('mat-dialog-container').should('be.visible');
    cy.get('mat-dialog-container').findBySel(`dialog-button-${label}`).click();
});

Then('after a while, I should see a secondary title {string}', (title: string) => {
    // Wait for spinner to disappear and check that it is gone
    cy.getBySel('loading-spinner', {timeout: 10000}).should('not.exist');
    // Check secondary title
    cy.checkElementTextExpected(title, cy.getBySel('secondary-title'), 'Admin secondary title');
    cy.screenshotAll();
});

Then('after a while, I should see a table with {int} recordings in it', (nbRecordings: number) => {
    // Wait for spinners to disappear and check that they are gone
    cy.getBySel('loading-spinner', {timeout: 10000}).should('not.exist');
    cy.getBySel('spinner-table-url-list', {timeout: 10000}).should('not.exist');
    // Check table and records
    cy.getBySel('table-url-list').should('be.visible');
    cy.getBySel('table-url-list').findBySel('row-table-url-list').should('have.length', nbRecordings);
    cy.screenshotAll();
});

Then('after a while, I should see a popup with title {string} and two buttons {string} and {string}', (title: string, button1: string, button2: string) => {
    // Wait for the popup to appear and check that it is visible
    cy.get('mat-dialog-container').should('be.visible');
    // Check popup title and buttons
    cy.checkElementTextExpected(title, cy.get('mat-dialog-container').findBySel('dialog-title'), 'Dialog title');
    cy.checkElementTextExpected(button1, cy.get('mat-dialog-container').findBySel('dialog-button-No'), 'Dialog button #1');
    cy.checkElementTextExpected(button2, cy.get('mat-dialog-container').findBySel('dialog-button-Yes'), 'Dialog button #2');
    cy.screenshotAll();
});

Then('after a while, I should not see the popup', () => {
    // Wait for the popup to disappear and check that it is not visible anymore
    cy.get('mat-dialog-container').should('not.exist');
    cy.screenshotAll();
});

Then('I should see an error message {string} in the table', (message: string) => {
    // Check that error message is visible
    cy.getBySel('error-table-url-list').should('be.visible');
    // Check error message
    cy.checkElementTextExpected(message, cy.getBySel('error-table-url-list'), 'Error message');
    cy.screenshotAll();
});

Then('short URL IDs of the visible recordings should be {string}', (shortUrlIds: string) => {
    cy.checkElementTextExpected(shortUrlIds, cy.getBySel('id-table-url-list'), 'Visible Short URL Ids');
});

Then('page information should be {string}', (pageInfo: string) => {
    cy.checkElementTextExpected(pageInfo, cy.getBySel('info-table-url-list'), 'Page information text');
});
