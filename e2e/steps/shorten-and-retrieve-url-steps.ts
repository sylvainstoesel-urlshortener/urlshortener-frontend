import { Then, When } from '@badeball/cypress-cucumber-preprocessor';

When('I click on input text inside feature {string}', (name: string) => {
    // Get feature index from feature name
    cy.findAccordionPanelSelectorForName(name).then((selector) => {
        // Check that input is visible before clicking on it.
        cy.getBySel(selector).findBySel('form-text-input').should('be.visible');
        // Click on input and wait a bit
        cy.getBySel(selector).findBySel('form-text-input').click();
        cy.screenshotAll();
      });
});

When('I type the value {string} in input text inside feature {string}', (value: string, name: string) => {
    // Get feature index from feature name
    cy.findAccordionPanelSelectorForName(name).then((selector) => {
        // Check that input is visible before typing in it.
        cy.getBySel(selector).findBySel('form-text-input').should('be.visible');
        // Write text inside the input (do nothing if it's an empty text).
        if (value && value.length > 0) {
            cy.getBySel(selector).findBySel('form-text-input').type(value);
        }
        cy.screenshotAll();
      });
});

When('I click on the submit button {string} inside feature {string}', (buttonLabel: string, name: string) => {
    // Get feature index from feature name
    cy.findAccordionPanelSelectorForName(name).then((selector) => {
        // Check that button is visible before clicking on it.
        cy.getBySel(selector).findBySel('form-submit-button').should('be.visible');
        // Check button label
        cy.checkElementTextExpected(buttonLabel, cy.getBySel(selector).findBySel('form-submit-button'), 'Button label');
        // Click on button and wait a bit
        cy.getBySel(selector).findBySel('form-submit-button').click();
        cy.screenshotAll();
      });
});

  
Then('after a while, I should see a label saying {string} inside feature {string}', (label: string, name: string) => {
    // Get feature index from feature name
    cy.findAccordionPanelSelectorForName(name).then((selector) => {
        // Wait for spinner to disappear and check that it is gone
        cy.getBySel(selector).findBySel('form-spinner').should('not.exist');
        // Check that input is not there anymore
        cy.getBySel(selector).findBySel('form-text-input').should('not.exist');
        // Check label
        cy.checkElementTextExpected(label, cy.getBySel(selector).findBySel('form-response-label'), 'Error message');
        cy.screenshotAll();
      });
});


Then('I should see a link inside feature {string} with value {string}', (name: string, link: string) => {
    // Get feature index from feature name
    cy.findAccordionPanelSelectorForName(name).then((selector) => {
        // Check that spinner is gone
        cy.getBySel(selector).findBySel('form-spinner').should('not.exist');
        // Check that input is not there anymore
        cy.getBySel(selector).findBySel('form-text-input').should('not.exist');
        // Check link obtained
        cy.getBySel(selector).findBySel('form-response-link').should('be.visible');
        cy.checkElementTextExpected(link, cy.getBySel(selector).findBySel('form-response-link'), 'Error message');
      });
});
  
Then('after a while, I should see an error message {string} below the input text inside feature {string}', (error: string, name: string) => {
    // Get feature index from feature name
    cy.findAccordionPanelSelectorForName(name).then((selector) => {
        // Wait for spinner to disappear and check that it is gone
        cy.wait(1000);
        cy.getBySel(selector).findBySel('form-spinner').should('not.exist');
        // Check that input is still there
        cy.getBySel(selector).findBySel('form-text-input').should('be.visible');
        // Check error message
        cy.checkElementTextExpected(error, cy.getBySel(selector).findBySel('form-error-message'), 'Server error message');
      });
});
