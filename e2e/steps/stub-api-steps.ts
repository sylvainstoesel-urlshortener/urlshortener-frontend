import { Given, Then, attach } from '@badeball/cypress-cucumber-preprocessor';


/**
 * UTILS Constants & Methods
 */
const ENDPOINT_URL_PARAMETERS = {name: 'ENDPOINT_URL_PARAMETERS', request: {method: 'GET', pathname: '/urlparameters'}};
const ENDPOINT_SHORTEN_URL = {name: 'ENDPOINT_SHORTEN_URL', request: {method: 'POST', pathname: '/urldetails'}};
const ENDPOINT_SEARCH_URL = {name: 'ENDPOINT_SEARCH_URL', request: {method: 'GET', pathname: '/urlsearch'}};
const ENDPOINT_GET_URL = {name: 'ENDPOINT_GET_URL', request: {method: 'GET', pathname: '/urldetails/*'}};
const ENDPOINT_LIST_URLS = {name: 'ENDPOINT_LIST_URLS', request: {method: 'GET', pathname: '/urlsearch/list'}};
const ENDPOINT_DELETE_URL = {name: 'ENDPOINT_DELETE_URL', request: {method: 'DELETE', pathname: '/urldetails/*'}}; 

function addStub(endpoint : {name: string, request : {method: string, pathname: string}},
                 response : {statusCode: number}) {
  cy.intercept(endpoint.request, response).as(endpoint.name);
  attach(`API Stub added: ${endpoint.name}\n` +
          `Request:\n${JSON.stringify(endpoint.request, null, 2)}\n` +
          `Response:\n${JSON.stringify(response, null, 2)}`);
}

function addErrorStub(endpoint : {name: string, request : {method: string, pathname: string}},
                      httpStatus: number, message: string, delayMilliseconds: number) {
  const response = {
    delay: delayMilliseconds,
    statusCode: httpStatus,
    body: {
      status: httpStatus,
      code: "ERROR_CODE",
      message: message
    },
    times: 2
  };
  addStub(endpoint, response);
}

/**
 * DEFINITIONS OF STEPS RELATED TO STUBS & MOCKS OF SERVER
 */
Given(
  'URL-parameters API endpoint is ready to answer within {int}ms if called, with domain={string}, ID size={int} and API version={string}',
  (
    delayMilliseconds: number,
    url: string,
    size: number,
    apiVersion: string
  ) => {
    const response = {
      delay: delayMilliseconds,
      statusCode: 200,
      body: {
        shortUrlIdSize: size,
        shortUrlDomain: url,
        apiVersion
      },
      times: 1
    };
    addStub(ENDPOINT_URL_PARAMETERS, response);
  }
);

Given(
  'URL-parameters API endpoint returns an HTTP {int} error within {int}ms if called, with error message={string}',
  (httpStatus: number, delayMilliseconds: number, message: string) => {
    addErrorStub(ENDPOINT_URL_PARAMETERS, httpStatus, message, delayMilliseconds);
  }
);

Given(
  'shorten-URL API endpoint is ready to answer within {int}ms if called, with generated ID={string} and domain={string}',
  (delayMilliseconds: number, generatedShortUrlId: string, domain: string) => {
    const response = {
      delay: delayMilliseconds,
      statusCode: 201,
      body: {
        shortUrlId: generatedShortUrlId,
        shortUrl: domain + generatedShortUrlId,
        longUrl: 'original-url.com',
        creationTimestamp: '2021-06-23T02:50:15.006Z'
      },
      times: 1
    };
    addStub(ENDPOINT_SHORTEN_URL, response);
  }
);

Given(
  'shorten-URL API endpoint returns an HTTP {int} error within {int}ms if called, with error message={string}',
  (httpStatus: number, delayMilliseconds: number, message: string) => {
    addErrorStub(ENDPOINT_SHORTEN_URL, httpStatus, message, delayMilliseconds);
  }
);

Given(
  'search-URL API endpoint is ready to answer within {int}ms if called, with long URL={string}',
  (delayMilliseconds: number, longUrl: string) => {
    const response = {
      delay: delayMilliseconds,
      statusCode: 200,
      body: {
        shortUrlId: '0000000000',
        shortUrl: 'short.com/0000000000',
        longUrl,
        creationTimestamp: '2021-06-23T02:50:15.006Z'
      },
      times: 1
    };
    addStub(ENDPOINT_SEARCH_URL, response);
  }
);

Given(
  'search-URL API endpoint returns an HTTP {int} error within {int}ms if called, with error message={string}',
  (httpStatus: number, delayMilliseconds: number, message: string) => {
    addErrorStub(ENDPOINT_SEARCH_URL, httpStatus, message, delayMilliseconds);
  }
);

Given(
  'list-URLs API endpoint is ready to answer within {int}ms if called, with a list containing a total of {int} URL details objects',
  (delayMilliseconds: number, nbObjects: number) => {
    if (nbObjects > 0) {
      for (let indexStart = 0 ; indexStart < nbObjects ; indexStart += 5) {
        const urls = [];
        for (let i = indexStart; i < Math.min(nbObjects, indexStart + 5); i++) {
          urls.push({
            shortUrlId: 'abcd' + String(i).padStart(6, '0'),
            shortUrl: 'domain.ca/abcd' + String(i).padStart(6, '0'),
            longUrl: 'original-url-' + i + '.com',
            creationTimestamp: '2021-06-23T00:00:' + String(i % 100).padStart(2, '0') + '.000Z'
          });
          const response = {
            delay: delayMilliseconds,
            statusCode: 200,
            body: urls,
            headers: {
              "x-page-number": String(Math.floor(indexStart / 5)),
              "x-page-size": "5",
              "x-total-results": String(nbObjects),
              "x-total-pages": String(Math.ceil(nbObjects / 5)),
            },
            times: 1
          };
          const request = {
            name: ENDPOINT_LIST_URLS.name,
            request: {
              method: ENDPOINT_LIST_URLS.request.method,
              pathname: ENDPOINT_LIST_URLS.request.pathname,
              query: {
                page: String(Math.floor(indexStart / 5))
              }
            }
          };
          addStub(request, response);
        }
      }
    } else {
      const response = {
        delay: delayMilliseconds,
        statusCode: 200,
        body: [],
        headers: {
          "x-page-number": "0",
          "x-page-size": "5",
          "x-total-results": "0",
          "x-total-pages": "0",
        },
        times: 1
      };
      addStub(ENDPOINT_LIST_URLS, response);
    }
  }
);

Given(
  'list-URLs API endpoint returns an HTTP {int} error within {int}ms if called, with error message={string}',
  (httpStatus: number, delayMilliseconds: number, message: string) => {
    addErrorStub(ENDPOINT_LIST_URLS, httpStatus, message, delayMilliseconds);
  }
);

Given(
  'delete-URL API endpoint is ready to answer within {int}ms if called',
  (delayMilliseconds: number) => {
    const response = {
      delay: delayMilliseconds,
      statusCode: 200,
      times: 1
    };
    addStub(ENDPOINT_DELETE_URL, response);
  }
);

Given(
  'delete-URL API endpoint returns an HTTP {int} error within {int}ms if called, with error message={string}',
  (httpStatus: number, delayMilliseconds: number, message: string) => {
    addErrorStub(ENDPOINT_DELETE_URL, httpStatus, message, delayMilliseconds);
  }
);

Given(
  'get-URL API endpoint is ready to answer within {int}ms if called, with long URL={string}',
  (delayMilliseconds: number, longUrl: string) => {
    const response = {
      delay: delayMilliseconds,
      statusCode: 200,
      body: {
        shortUrlId: '0000000000',
        shortUrl: 'short.com/0000000000',
        longUrl,
        creationTimestamp: '2021-06-23T02:50:15.006Z'
      },
      times: 1
    };
    addStub(ENDPOINT_GET_URL, response);
  }
);

Given(
  'get-URL API endpoint returns an HTTP {int} error within {int}ms if called, with error message={string}',
  (httpStatus: number, delayMilliseconds: number, message: string) => {
    addErrorStub(ENDPOINT_GET_URL, httpStatus, message, delayMilliseconds);
  }
);

/**
 * Verify that fake API has been called by the front-end at the end of the tests.
 */
Then(
  'URL-parameters API endpoint should have been called {int}x',
  (numberOfCalls: number) => {
    cy.get(`@${ENDPOINT_URL_PARAMETERS.name}.all`).then((calls: any) => {
      attach(`${calls.length} call(s) made on ${ENDPOINT_URL_PARAMETERS.name}: ${JSON.stringify(calls, null, 2)}`);
      cy.wrap(calls.length).should('equal', numberOfCalls)
    });
  }
);

Then(
  'shorten-URL API endpoint should have been called {int}x with long URL {string}',
  (numberOfCalls: number, longUrl?: string) => {
    cy.get(`@${ENDPOINT_SHORTEN_URL.name}.all`).then((calls: any) => {
      attach(`${calls.length} call(s) made on ${ENDPOINT_SHORTEN_URL.name}: ${JSON.stringify(calls, null, 2)}`);
      cy.wrap(calls.filter((c: any) => c.request.body.value === longUrl).length).should('equal', numberOfCalls);
    });
  }
);

Then(
  'shorten-URL API endpoint should have been called {int}x',
  (numberOfCalls: number) => {
    cy.get(`@${ENDPOINT_SHORTEN_URL.name}.all`).then((calls: any) => {
      attach(`${calls.length} call(s) made on ${ENDPOINT_SHORTEN_URL.name}: ${JSON.stringify(calls, null, 2)}`);
      cy.wrap(calls.length).should('equal', numberOfCalls)
    });
  }
);

Then(
  'search-URL API endpoint should have been called {int}x with short URL {string}',
  (numberOfCalls: number, shortUrl: string) => {
    cy.get(`@${ENDPOINT_SEARCH_URL.name}.all`).then((calls: any) => {
      attach(`${calls.length} call(s) made on ${ENDPOINT_SEARCH_URL.name}: ${JSON.stringify(calls, null, 2)}`);
      cy.wrap(calls.filter((c: any) => c.request.url.endsWith(`/urlsearch?value=${shortUrl.replace('/', '%2F')}`)).length).should('equal', numberOfCalls);
    });
  }
);

Then(
  'search-URL API endpoint should have been called {int}x',
  (numberOfCalls: number) => {
    cy.get(`@${ENDPOINT_SEARCH_URL.name}.all`).then((calls: any) => {
      attach(`${calls.length} call(s) made on ${ENDPOINT_SEARCH_URL.name}: ${JSON.stringify(calls, null, 2)}`);
      cy.wrap(calls.length).should('equal', numberOfCalls)
    });
  }
);

Then(
  'list-URLs API endpoint should have been called {int}x with page index {int}',
  (numberOfCalls: number, pageIndex?: number) => {
    cy.get(`@${ENDPOINT_LIST_URLS.name}.all`).then((calls: any) => {
      attach(`${calls.length} call(s) made on ${ENDPOINT_LIST_URLS.name}: ${JSON.stringify(calls, null, 2)}`);
      cy.wrap(calls.filter((c: any) => c.request.url.includes(`page=${pageIndex}`)).length).should('equal', numberOfCalls);
    });
  }
);

Then(
  'list-URLs API endpoint should have been called {int}x',
  (numberOfCalls: number) => {
    cy.get(`@${ENDPOINT_LIST_URLS.name}.all`).then((calls) => {
      attach(`${calls.length} call(s) made on ${ENDPOINT_LIST_URLS.name}: ${JSON.stringify(calls, null, 2)}`);
      cy.wrap(calls.length).should('equal', numberOfCalls)
    });
  }
);

Then(
  'delete-URL API endpoint should have been called {int}x with short URL ID {string}',
  (numberOfCalls: number, shortUrlId?: string) => {
    cy.get(`@${ENDPOINT_DELETE_URL.name}.all`).then((calls: any) => {
      attach(`${calls.length} call(s) made on ${ENDPOINT_DELETE_URL.name}: ${JSON.stringify(calls, null, 2)}`);
      cy.wrap(calls.filter((c: any) => c.request.url.endsWith('/urldetails/' + shortUrlId)).length).should('equal', numberOfCalls);
    });
  }
);

Then(
  'delete-URL API endpoint should have been called {int}x',
  (numberOfCalls: number) => {
    cy.get(`@${ENDPOINT_DELETE_URL.name}.all`).then((calls) => {
      attach(`${calls.length} call(s) made on ${ENDPOINT_DELETE_URL.name}: ${JSON.stringify(calls, null, 2)}`);
      cy.wrap(calls.length).should('equal', numberOfCalls)
    });
  }
);

Then(
  'get-URL API endpoint should have been called {int}x with short URL ID {string}',
  (numberOfCalls: number, shortUrlId?: string) => {
    cy.get(`@${ENDPOINT_GET_URL.name}.all`).then((calls: any) => {
      attach(`${calls.length} call(s) made on ${ENDPOINT_GET_URL.name}: ${JSON.stringify(calls, null, 2)}`);
      cy.wrap(calls.filter((c: any) => c.request.url.endsWith('/urldetails/' + shortUrlId)).length).should('equal', numberOfCalls);
    });
  }
);

Then(
  'get-URL API endpoint should have been called {int}x',
  (numberOfCalls: number) => {
    cy.get(`@${ENDPOINT_GET_URL.name}.all`).then((calls) => {
      attach(`${calls.length} call(s) made on ${ENDPOINT_GET_URL.name}: ${JSON.stringify(calls, null, 2)}`);
      cy.wrap(calls.length).should('equal', numberOfCalls)
    });
  }
);
