import { When, Then, attach } from '@badeball/cypress-cucumber-preprocessor';


When('I navigate to the main page', () => {
  const URI = '/';
  attach(`Navigating to ${Cypress.config('baseUrl') + URI}`);
  cy.visit(URI, {
    onBeforeLoad(win) {
      cy.spy(win.console, 'error').as('consoleError');
    },
  });
});

Then('browser title and app main title should be {string}', (title: string) => {
  // Check the title of the browser window
  cy.title().should('eq', title);
  // Check main title of the app (h1 element)
  cy.getBySel('app-title').should('have.text', title);
  cy.screenshotAll();
});

Then('app main title is a link that should redirect to {string}', (redirectLink: string) => {
  cy.getBySel('app-title').should('have.attr', 'href').and('eq', redirectLink);
  cy.screenshotAll();
});

Then('I should see the spinner', () => {
  // Check that spinner is shown
  cy.getBySel('loading-spinner').should('be.visible');
  cy.screenshotAll();
});

Then('after a while, I should see some text below the spinner', (ms: number) => {
  // Check that spinner is still here
  cy.getBySel('loading-spinner').should('be.visible');
  // Check that there is a text below the spinner
  cy.getBySel('loading-text').should('be.visible');
  cy.screenshotAll();
});

Then('after a while, I should see the red error message {string}', (error: string) => {
  // Wait for spinner to disappear and check that it is gone
  cy.getBySel('loading-spinner', {timeout: 10000}).should('not.exist');
  // Check error message
  cy.checkElementTextExpected(error, cy.getBySel('app-error'), 'Error message');
  cy.screenshotAll();
});

Then('I should see the footer with 4 links {string}, {string}, {string}, {string}',
  (lnk1: string, lnk2: string, lnk3: string, lnk4: string) => {
    cy.checkElementTextExpected(lnk1, cy.getBySel('footer-link-1'), 'Footer link label #1');
    cy.checkElementTextExpected(lnk2, cy.getBySel('footer-link-2'), 'Footer link label #2');
    cy.checkElementTextExpected(lnk3, cy.getBySel('footer-link-3'), 'Footer link label #3');
    cy.checkElementTextExpected(lnk4, cy.getBySel('footer-link-4'), 'Footer link label #4');
});

Then('link #{int} in footer should redirect to {string}', (n: number, redirectLink: string) => {
  cy.getBySel(`footer-link-${n}`).should('have.attr', 'href').then((href) => {
    cy.checkValueExpected(redirectLink, href, `Footer link #${n}`);
  });
});

Then(
  'if I click on link #{int} in footer, I should see a menu with 4 other languages {string}, {string}, {string}, {string}',
  (n: number, lng1: string, lng2: string, lng3: string, lng4: string) => {
    // Check that menu is not shown before clicking on the link
    cy.getByDataTestClass('footer-language-menu').should('not.exist');
    // Click on the link
    cy.getBySel(`footer-link-${n}`).click();
    // Check that menu is shown with the 4 languages
    cy.getByDataTestClass('footer-language-menu').should('be.visible');
    cy.getByDataTestClass('footer-language-menu').find('button').should('be.visible');
    cy.checkElementTextExpected(lng1, cy.getByDataTestClass('footer-language-menu').find('button:nth-child(1)'), 'Language #1');
    cy.checkElementTextExpected(lng2, cy.getByDataTestClass('footer-language-menu').find('button:nth-child(2)'), 'Language #2');
    cy.checkElementTextExpected(lng3, cy.getByDataTestClass('footer-language-menu').find('button:nth-child(3)'), 'Language #3');
    cy.checkElementTextExpected(lng4, cy.getByDataTestClass('footer-language-menu').find('button:nth-child(4)'), 'Language #4');
    cy.screenshotAll();
    // Close the language menu
    cy.get('.cdk-overlay-backdrop').click({force: true});
  }
);

Then(
  'if I click on link #{int} in footer, I should see a menu with 3 options {string}, {string}, {string}',
  (n: number, opt1: string, opt2: string, opt3: string) => {
    // Check that menu is not shown before clicking on the link
    cy.getByDataTestClass('footer-copyright-menu').should('not.exist');
    // Click on the link
    cy.getBySel(`footer-link-${n}`).click();
    // Check that menu is shown with the 3 options
    cy.getByDataTestClass('footer-copyright-menu').should('be.visible');
    cy.getByDataTestClass('footer-copyright-menu').find('a').should('be.visible');
    cy.checkElementTextExpected(opt1, cy.getByDataTestClass('footer-copyright-menu').find('a:nth-child(1)'), 'Copyright Option #1');
    cy.checkElementTextExpected(opt2, cy.getByDataTestClass('footer-copyright-menu').find('a:nth-child(2)'), 'Copyright Option #2');
    cy.checkElementTextExpected(opt3, cy.getByDataTestClass('footer-copyright-menu').find('a:nth-child(3)'), 'Copyright Option #3');
    cy.screenshotAll();
    // Close the Copyright menu
    cy.get('.cdk-overlay-backdrop').click({force: true});
  }
);

Then('no error should be printed in browser console', () => {
  cy.get('@consoleError')
  .invoke('getCalls')
  .then((calls : Array<any>) => {
    if (calls && calls.length > 0) {
      attach(JSON.stringify(calls, null, 2));
    } else {
      attach('Console error output is empty.');
    }
    expect(calls).to.be.empty;
  })
});

Then('the error message {string} should be printed in browser console', (errorMessage) => {
  cy.get('@consoleError')
  .invoke('getCalls')
  .then((calls : Array<any>) => {
    if (calls && calls.length > 0) {
      attach(JSON.stringify(calls, null, 2));
    } else {
      attach('Console error output is empty.');
    }
    expect(calls).not.to.be.empty;
    expect(calls.map(c => c.lastArg)).to.include(errorMessage);
  });
});
