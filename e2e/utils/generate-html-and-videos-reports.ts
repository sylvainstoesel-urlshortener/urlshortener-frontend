const REPORTS_FOLDER = 'e2e-reports/';
const VIDEOS_FOLDER = `${REPORTS_FOLDER}videos/`;
const HTML_REPORT_FILE_NAME = 'cucumber-report.html';
const HTML_VIDEO_REPORT_FILE_NAME = 'index.html';

const fs = require('fs');

function getVideosList() : string[] {
  let result: string[] = [];
  if (!fs.existsSync(VIDEOS_FOLDER)){
    fs.mkdirSync(VIDEOS_FOLDER);
  }
  fs.readdirSync(VIDEOS_FOLDER).forEach((file : string) => {
    if (file.endsWith('.mp4')) {
      result.push(file);
    }
  });
  return result;
}


function generateVideoHtml(index: number, file: string): string {
  return `
        <div id="videos${index}" class="video">
          <a onclick="loadVideo('videos${index}', '${file}')">Feature: ${file.replace('.mp4', '')}</a>
        </div>
  `;
}

function generateHtmlContent() {
  const videos = getVideosList();
  const videosContent = videos.length == 0 ?
    '<h3>No video were generated. Is Cypress video recording disabled?</h3>' :
    videos.map((video, index) => generateVideoHtml(index, video)).join('');
  const content = `
    <html>
      <head>
        <title>Cypress / Cucumber Videos Report</title>
        <style>
          html {
              overflow: -moz-scrollbars-vertical; 
              overflow-y: scroll;
          }
          body {
            font-family: "Times New Roman", Times, serif;
          }
          h1, h2, h3, a, div, small {
            text-align: center;
          }
          a {
            display: block;
            font-size: 24;
            font-weight: bold;
            cursor: hand;
            color: darkblue;
          }
          h3 {
            color: #833
          }
          div.video {
            background-color: #DEF;
            margin: 20px 5%;
            padding: 10px;
          }
          div.video:hover {
            background-color: #CDF;
          }
          small {
            display: block;
            color: darkgray;
          }
        </style>
        <script>
          function loadVideo(divId, file) {
            const divVideo = document.getElementById(divId);

            if (divVideo.children.length == 1) {
              const source = document.createElement("source");
              source.setAttribute("src", file);
              source.setAttribute("type", "video/mp4");
              const video = document.createElement("video");
              video.setAttribute("id", "inner" + divId);
              video.setAttribute("width", "460");
              video.setAttribute("height", "320");
              video.setAttribute("controls", "");
              video.appendChild(source);
              divVideo.appendChild(video);
            }
            else {
              divVideo.removeChild(document.getElementById("inner" + divId));
            }
          }
        </script>
      </head>
      <body>
        <h1>Cypress / Cucumber Report</h1>
        <h2>Recorded videos during the execution of scenarios</h2>
        <small>Report generated on ${new Date()}</small>
        ${videosContent}
      </body>
    </html>`;

  fs.writeFile(VIDEOS_FOLDER + HTML_VIDEO_REPORT_FILE_NAME, content, (err: any) => {
    if (err) {
      console.error(err);
    }
  });
}

/**
 * EXECUTION - Generation of index.html file in the /videos/ folder.
 */
console.log(`Generating Video report '${HTML_VIDEO_REPORT_FILE_NAME}' within ${VIDEOS_FOLDER} folder...`);
generateHtmlContent();
console.log('Done!');

console.log(`Generating HTML report '${HTML_REPORT_FILE_NAME}' within ${REPORTS_FOLDER} folder...`);
fs.rmSync(`${REPORTS_FOLDER}downloads/`, { recursive: true, force: true });
fs.rmSync(`${REPORTS_FOLDER}screenshots/`, { recursive: true, force: true });
fs.rmSync(`${REPORTS_FOLDER}cucumber-messages.ndjson`)
require('cucumber-html-reporter').generate({
  theme: 'bootstrap',
  jsonFile: 'e2e-reports/cucumber-report.json',
  output: 'e2e-reports/cucumber-report.html',
  reportSuiteAsScenarios: true,
  scenarioTimestamp: true,
  launchReport: true,
  failedSummaryReport: true,
  noInlineScreenshots: true,
  storeScreenshots: true
});
console.log('Done!');