# language: en
Feature: 4. Redirecting short URL to long URL screen

    Scenario: 01 Redirecting screen should redirect to long URL
        Given URL-parameters API endpoint is ready to answer within 1000ms if called, with domain="short.com/", ID size=10 and API version="1.2.3"
        And get-URL API endpoint is ready to answer within 1000ms if called, with long URL="https://gitlab.com/sylvainstoesel-urlshortener"
        When I navigate to the redirection page with short URL ID "abcd000000"
        Then browser title and app main title should be "Sylvain Stoesel’s Url Shortener"
        And after a while, I should see a redirection message saying "Retrieving your URL..."
        And after a while, I should see a redirection message starting with "Redirecting to gitlab.com/sylvainstoesel-urlshortener"
        And no error should be printed in browser console
        And after 4 seconds, I should be redirected to URL "https://gitlab.com/sylvainstoesel-urlshortener"
        And get-URL API endpoint should have been called 1x with short URL ID "abcd000000"
        And URL-parameters API endpoint should have been called 1x

    Scenario: 02 Redirecting screen should show an error when API returns an error
        Given URL-parameters API endpoint returns an HTTP 500 error within 100ms if called, with error message="Server-side error!"
        And get-URL API endpoint is ready to answer within 100ms if called, with long URL="https://www.will-not-be-called.com"
        When I navigate to the redirection page with short URL ID "abcd000000"
        Then browser title and app main title should be "Sylvain Stoesel’s Url Shortener"
        And I should see the spinner
        And after a while, I should see the red error message "Server-side error!"
        And the error message "STATUS 500 WHEN CONTACTING API: Server-side error!" should be printed in browser console
        And URL-parameters API endpoint should have been called 2x
        And get-URL API endpoint should have been called 0x

    Scenario: 03 Redirecting screen should show an error when API returns an error when retrieving the URL
        Given URL-parameters API endpoint is ready to answer within 1000ms if called, with domain="short.com/", ID size=10 and API version="1.2.3"
        And get-URL API endpoint returns an HTTP 500 error within 800ms if called, with error message="API error when retrieving URL!"
        When I navigate to the redirection page with short URL ID "abcd000000"
        Then browser title and app main title should be "Sylvain Stoesel’s Url Shortener"
        And after a while, I should see a redirection message saying "Retrieving your URL..."
        And after a while, I should see an error "API error when retrieving URL!" instead of the redirection message
        And the error message "STATUS 500 WHEN CONTACTING API: API error when retrieving URL!" should be printed in browser console
        And get-URL API endpoint should have been called 2x with short URL ID "abcd000000"
        And URL-parameters API endpoint should have been called 1x
