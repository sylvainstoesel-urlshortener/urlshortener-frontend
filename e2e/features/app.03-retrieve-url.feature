# language: en
Feature: 3. Retrieving long URLs

    Background: I open the 'Find back a URL' feature
        Given URL-parameters API endpoint is ready to answer within 100ms if called, with domain="short.com/", ID size=10 and API version="1.2.3"
        When I navigate to the main page
        Then after a while, I should see the closed accordion with the 2 features "Shorten a URL" and "Find back a URL"
        And if I click on feature "Find back a URL", it opens the feature whose label is "Please enter a short URL here to find back its original version."
        And no error should be printed in browser console
        And URL-parameters API endpoint should have been called 1x

    Scenario: 01 Long URL should be properly retrieved from a valid short URL
        Given search-URL API endpoint is ready to answer within 100ms if called, with long URL="www.this-is-a-long-url.com"
        When I click on input text inside feature "Find back a URL"
        And I type the value "short.com/1a2b3c4d5e" in input text inside feature "Find back a URL"
        And I click on the submit button "Retrieve" inside feature "Find back a URL"
        Then after a while, I should see a label saying "Your original URL" inside feature "Find back a URL"
        And I should see a link inside feature "Find back a URL" with value "www.this-is-a-long-url.com"
        And no error should be printed in browser console
        And search-URL API endpoint should have been called 1x with short URL "short.com/1a2b3c4d5e"

    Scenario: 02 Long URL should not be retrieved from empty short URL
        Given search-URL API endpoint is ready to answer within 100ms if called, with long URL="www.this-is-a-long-url.com"
        When I click on input text inside feature "Find back a URL"
        And I type the value "" in input text inside feature "Find back a URL"
        And I click on the submit button "Retrieve" inside feature "Find back a URL"
        Then after a while, I should see an error message "Short URL cannot be empty." below the input text inside feature "Find back a URL"
        And no error should be printed in browser console
        And search-URL API endpoint should have been called 0x

    Scenario: 03 Long URL should not be retrieved from invalid short URL
        Given search-URL API endpoint is ready to answer within 100ms if called, with long URL="www.this-is-a-long-url.com"
        When I click on input text inside feature "Find back a URL"
        And I type the value "invalidlongurl" in input text inside feature "Find back a URL"
        And I click on the submit button "Retrieve" inside feature "Find back a URL"
        Then after a while, I should see an error message "Short URL is not valid." below the input text inside feature "Find back a URL"
        And no error should be printed in browser console
        And search-URL API endpoint should have been called 0x

    Scenario: 04 Long URL cannot be retrieved because API returns an error
        Given search-URL API endpoint returns an HTTP 500 error within 100ms if called, with error message="Server-side error, unable to retrieve URL!"
        When I click on input text inside feature "Find back a URL"
        And I type the value "short.com/2b3c4d5e6f" in input text inside feature "Find back a URL"
        And I click on the submit button "Retrieve" inside feature "Find back a URL"
        Then after a while, I should see an error message "Server-side error, unable to retrieve URL!" below the input text inside feature "Find back a URL"
        And the error message "STATUS 500 WHEN CONTACTING API: Server-side error, unable to retrieve URL!" should be printed in browser console
        And search-URL API endpoint should have been called 2x with short URL "short.com/2b3c4d5e6f"
