# language: en
Feature: 6. Navigating and using features of admin screen

    Background: I open the admin screen - API is working properly
        Given URL-parameters API endpoint is ready to answer within 100ms if called, with domain="short.com/", ID size=10 and API version="1.2.3"
        Given list-URLs API endpoint is ready to answer within 800ms if called, with a list containing a total of 12 URL details objects
        When I navigate to the admin page
        Then after a while, I should see a secondary title "Administration"
        And after a while, I should see a table with 5 recordings in it
        And short URL IDs of the visible recordings should be "abcd000000  abcd000001  abcd000002  abcd000003  abcd000004"
        And page information should be "Page 1 on 3 (12 results)"
        And URL-parameters API endpoint should have been called 1x

    Scenario: 01 Admin screen should display all the recorded short URLs, 5 rows per page of results
        When I click on the "Next" page button
        Then after a while, I should see a table with 5 recordings in it
        And short URL IDs of the visible recordings should be "abcd000005  abcd000006  abcd000007  abcd000008  abcd000009"
        And page information should be "Page 2 on 3 (12 results)"
        When I click on the "Next" page button
        Then after a while, I should see a table with 2 recordings in it
        And short URL IDs of the visible recordings should be "abcd000010  abcd000011"
        And page information should be "Page 3 on 3 (12 results)"
        When I click on the "Previous" page button
        Then after a while, I should see a table with 5 recordings in it
        And short URL IDs of the visible recordings should be "abcd000005  abcd000006  abcd000007  abcd000008  abcd000009"
        And page information should be "Page 2 on 3 (12 results)"
        When I click on the "Previous" page button
        Then after a while, I should see a table with 5 recordings in it
        And short URL IDs of the visible recordings should be "abcd000000  abcd000001  abcd000002  abcd000003  abcd000004"
        And page information should be "Page 1 on 3 (12 results)"
        And no error should be printed in browser console
        And list-URLs API endpoint should have been called 1x with page index 0
        And list-URLs API endpoint should have been called 1x with page index 1
        And list-URLs API endpoint should have been called 1x with page index 2

    Scenario: 02 Clicking on the "trash bin" button should show the confirmation popup - Clicking on "No" should do nothing
        Given delete-URL API endpoint is ready to answer within 200ms if called
        When I click on the trash button of the visible record #1
        Then after a while, I should see a popup with title "Delete this short URL?" and two buttons "No" and "Yes"
        When I click on the "No" button inside the popup
        Then after a while, I should not see the popup
        And after a while, I should see a table with 5 recordings in it
        And delete-URL API endpoint should have been called 0x
        And no error should be printed in browser console

    Scenario: 03 Clicking on the "trash bin" button should show the confirmation popup - Clicking on "Yes" should delete URL
        Given delete-URL API endpoint is ready to answer within 200ms if called
        When I click on the trash button of the visible record #1
        Then after a while, I should see a popup with title "Delete this short URL?" and two buttons "No" and "Yes"
        When I click on the "Yes" button inside the popup
        Then after a while, I should not see the popup
        And after a while, I should see a table with 5 recordings in it
        And delete-URL API endpoint should have been called 1x with short URL ID "abcd000000"
        And no error should be printed in browser console

    Scenario: 04 Trying to delete a URL should show an error when API returns an error
        Given delete-URL API endpoint returns an HTTP 500 error within 100ms if called, with error message="API error!"
        When I click on the trash button of the visible record #1
        Then after a while, I should see a popup with title "Delete this short URL?" and two buttons "No" and "Yes"
        When I click on the "Yes" button inside the popup
        Then after a while, I should not see the popup
        And after a while, I should see a table with 5 recordings in it
        And I should see an error message "API error!" in the table
        And delete-URL API endpoint should have been called 2x with short URL ID "abcd000000"
        And the error message "STATUS 500 WHEN CONTACTING API: API error!" should be printed in browser console

