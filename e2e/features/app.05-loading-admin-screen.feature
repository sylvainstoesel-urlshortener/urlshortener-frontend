# language: en
Feature: 5. Loading and displaying the admin screen with all available features

    Scenario: 01 Admin screen should be properly displayed and all the features should be visible
        Given URL-parameters API endpoint is ready to answer within 500ms if called, with domain="localhost:4200/", ID size=10 and API version="1.2.3"
        Given list-URLs API endpoint is ready to answer within 1ms if called, with a list containing a total of 0 URL details objects
        When I navigate to the admin page
        Then browser title and app main title should be "Sylvain Stoesel’s Url Shortener"
        And app main title is a link that should redirect to "/"
        And after a while, I should see a secondary title "Administration"
        And after a while, I should see a table with 0 recordings in it
        And page information should be "Page 1 on 0 (no results)"
        And I should see the footer with 4 links "English", "REST API", "Administration", "© 2024 Sylvain Stoesel"
        And if I click on link #1 in footer, I should see a menu with 4 other languages "French", "Spanish", "Italian", "German"
        And link #2 in footer should redirect to "http://localhost:8080/swagger-ui.html"
        And link #3 in footer should redirect to "/admin"
        And if I click on link #4 in footer, I should see a menu with 3 options "Open-source project on Gitlab", "Backend project (API v1.2.3)", "Frontend project (SPA v1.9.0)"
        And no error should be printed in browser console
        And list-URLs API endpoint should have been called 1x with page index 0
        And URL-parameters API endpoint should have been called 1x

    Scenario: 02 Admin screen should show an error when API returns an error
        Given URL-parameters API endpoint returns an HTTP 500 error within 500ms if called, with error message="Server-side error!"
        When I navigate to the admin page
        Then browser title and app main title should be "Sylvain Stoesel’s Url Shortener"
        And I should see the spinner
        And after a while, I should see the red error message "Server-side error!"
        And the error message "STATUS 500 WHEN CONTACTING API: Server-side error!" should be printed in browser console
        And URL-parameters API endpoint should have been called 2x
        
    Scenario: 03 The table of records should show an error when API returns an error when listing URLs
        Given URL-parameters API endpoint is ready to answer within 500ms if called, with domain="localhost:4200/", ID size=10 and API version="1.2.3"
        Given list-URLs API endpoint returns an HTTP 500 error within 100ms if called, with error message="API Error, cannot list URLs!"
        When I navigate to the admin page
        Then browser title and app main title should be "Sylvain Stoesel’s Url Shortener"
        And after a while, I should see a secondary title "Administration"
        And after a while, I should see a table with 0 recordings in it
        And page information should be "Page 1 on 0 (no results)"
        And I should see an error message "API Error, cannot list URLs!" in the table
        And the error message "STATUS 500 WHEN CONTACTING API: API Error, cannot list URLs!" should be printed in browser console
        And URL-parameters API endpoint should have been called 1x
        And list-URLs API endpoint should have been called 2x with page index 0