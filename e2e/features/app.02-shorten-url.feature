# language: en
Feature: 2. Shortening long URLs

    Background: I open the 'Shorten URL' feature
        Given URL-parameters API endpoint is ready to answer within 100ms if called, with domain="short.com/", ID size=10 and API version="1.2.3"
        When I navigate to the main page
        Then after a while, I should see the closed accordion with the 2 features "Shorten a URL" and "Find back a URL"
        And if I click on feature "Shorten a URL", it opens the feature whose label is "Please enter any URL here to make it shorter."
        And URL-parameters API endpoint should have been called 1x

    Scenario: 01 Short URL should be properly generated from a valid long URL
        Given shorten-URL API endpoint is ready to answer within 100ms if called, with generated ID="00aa00aa01" and domain="short.com/"
        When I click on input text inside feature "Shorten a URL"
        And I type the value "valid-long-url.com" in input text inside feature "Shorten a URL"
        And I click on the submit button "Shorten" inside feature "Shorten a URL"
        Then after a while, I should see a label saying "Your shortened URL" inside feature "Shorten a URL"
        And I should see a link inside feature "Shorten a URL" with value "short.com/00aa00aa01"
        And no error should be printed in browser console
        And shorten-URL API endpoint should have been called 1x with long URL "valid-long-url.com"

    Scenario: 02 Short URL should not be generated from empty long URL
        Given shorten-URL API endpoint is ready to answer within 100ms if called, with generated ID="00aa00aa01" and domain="short.com/"
        When I click on input text inside feature "Shorten a URL"
        And I type the value "" in input text inside feature "Shorten a URL"
        And I click on the submit button "Shorten" inside feature "Shorten a URL"
        Then after a while, I should see an error message "URL cannot be empty." below the input text inside feature "Shorten a URL"
        And no error should be printed in browser console
        And shorten-URL API endpoint should have been called 0x

    Scenario: 03 Short URL should not be generated from invalid long URL
        Given shorten-URL API endpoint is ready to answer within 100ms if called, with generated ID="00aa00aa01" and domain="short.com/"
        When I click on input text inside feature "Shorten a URL"
        And I type the value "invalidlongurl" in input text inside feature "Shorten a URL"
        And I click on the submit button "Shorten" inside feature "Shorten a URL"
        Then after a while, I should see an error message "URL is not valid." below the input text inside feature "Shorten a URL"
        And no error should be printed in browser console
        And shorten-URL API endpoint should have been called 0x

    Scenario: 04 Short URL cannot be generated because API returns an error
        Given shorten-URL API endpoint returns an HTTP 500 error within 100ms if called, with error message="Server-side error, unable to shorten URL!"
        When I click on input text inside feature "Shorten a URL"
        And I type the value "valid-long-url-2.com" in input text inside feature "Shorten a URL"
        And I click on the submit button "Shorten" inside feature "Shorten a URL"
        Then after a while, I should see an error message "Server-side error, unable to shorten URL!" below the input text inside feature "Shorten a URL"
        And the error message "STATUS 500 WHEN CONTACTING API: Server-side error, unable to shorten URL!" should be printed in browser console
        And shorten-URL API endpoint should have been called 2x with long URL "valid-long-url-2.com"