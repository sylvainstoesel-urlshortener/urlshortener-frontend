# language: en
Feature: 1. Loading and displaying the main screen with all available features

    Scenario: 01 Main screen should be properly displayed and all the features should be visible
        Given URL-parameters API endpoint is ready to answer within 500ms if called, with domain="localhost:4200/", ID size=10 and API version="1.2.3"
        When I navigate to the main page
        Then browser title and app main title should be "Sylvain Stoesel’s Url Shortener"
        And app main title is a link that should redirect to "/"
        And after a while, I should see the closed accordion with the 2 features "Shorten a URL" and "Find back a URL"
        And if I click on feature "Shorten a URL", it opens the feature whose label is "Please enter any URL here to make it shorter."
        And if I click on feature "Find back a URL", it opens the feature whose label is "Please enter a short URL here to find back its original version."
        And I should see the footer with 4 links "English", "REST API", "Administration", "© 2024 Sylvain Stoesel"
        And if I click on link #1 in footer, I should see a menu with 4 other languages "French", "Spanish", "Italian", "German"
        And link #2 in footer should redirect to "http://localhost:8080/swagger-ui.html"
        And link #3 in footer should redirect to "/admin"
        And if I click on link #4 in footer, I should see a menu with 3 options "Open-source project on Gitlab", "Backend project (API v1.2.3)", "Frontend project (SPA v1.9.0)"
        And no error should be printed in browser console
        And URL-parameters API endpoint should have been called 1x

    Scenario: 02 Main screen should be properly loaded and displayed with a loading message when API is slow
        Given URL-parameters API endpoint is ready to answer within 8000ms if called, with domain="localhost:4200/", ID size=10 and API version="1.2.3"
        When I navigate to the main page
        Then browser title and app main title should be "Sylvain Stoesel’s Url Shortener"
        And I should see the spinner
        And after a while, I should see some text below the spinner
        And after a while, I should see the closed accordion with the 2 features "Shorten a URL" and "Find back a URL"
        And no error should be printed in browser console
        And URL-parameters API endpoint should have been called 1x

    Scenario: 03 Main screen should show an error when API returns an error
        Given URL-parameters API endpoint returns an HTTP 500 error within 500ms if called, with error message="Server-side error!"
        When I navigate to the main page
        Then browser title and app main title should be "Sylvain Stoesel’s Url Shortener"
        And I should see the spinner
        And after a while, I should see the red error message "Server-side error!"
        And the error message "STATUS 500 WHEN CONTACTING API: Server-side error!" should be printed in browser console
        And URL-parameters API endpoint should have been called 2x